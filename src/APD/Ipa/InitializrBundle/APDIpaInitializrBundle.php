<?php

namespace APD\Ipa\InitializrBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Extiende el paquete APDSaquitoInitializrBundle
 *
 * 
 * @category Bundle
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDIpaInitializrBundle extends Bundle
{
    public function getParent()
    {
        return 'APDSaquitoInitializrBundle';
    }
}
