<?php

namespace APD\Ipa\PropioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use APD\Ipa\PropioBundle\Entity\ipaDocumentos;
use APD\Saquito\PaginasBundle\Entity\saquitoPaginas;
use APD\Saquito\MainBundle\Utiles\utilesPDF;

/**
 * Controlador para los documentos del IPA
 *
 * @Route("/documentos")
 * 
 * @category Controladores
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class DocumentosController extends Controller
{

    /**
     * Armar Menu
     * 
     * @Route("/{tipo}/{id}/{slug}", name="ipa_documentos_ver")
     *  
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param ipaDocumentos $documento Documento a mostrar
     * @return string Código HTML de la página con el documento
     */
    public function verDocumentoAction(ipaDocumentos $documento)
    {
        $pagina = new saquitoPaginas;
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') or ($documento->getPrivado() == false)) {
            $pagina->setTitulo($documento->getTitulo());
            $pagina->setDescripcion($documento->getDescripcion());

            return $this->render(
                    'APDIpaPropioBundle:Documentos:ver.html.twig',array(
                        'documento' => $documento,
                        'pagina' => $pagina
                        ));
        } else {
            throw new \Exception('Esta información es sólo de uso interno del Instituto Provincial del Agua!');
        }     
    }

    /**
     * Armar Menu
     * 
     * @Route("/pdf/{tipo}/{id}/{slug}", name="ipa_documentos_pdf")
     *  
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param ipaDocumentos $documento Documento a mostrar
     * @return string Código HTML de la página con el documento
     */
    public function pdfDocumentoAction(ipaDocumentos $documento)
    {
//        $pagina = new saquitoPaginas;
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') or ($documento->getPrivado() == false)) {
            
            $miPdf = new utilesPDF();
            $miPdf->SetCreator(PDF_CREATOR);
            $miPdf->SetAuthor('Instituto Provincial del Agua Chubut');
            $miPdf->SetTitle($documento->getTitulo());
            $miPdf->SetSubject($documento->getDescripcion());
            $miPdf->Image('bundles/web/images/isologo.png',240,10,33);
            $miPdf->line(15,23,280,23);
            // set auto page breaks
            $miPdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            // set default header data
            $miPdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $miPdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $miPdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            $miPdf->AddPage();
            $miPdf->WriteHTML( '<h1>'. $documento->getTitulo() . '</h1>' . $documento->getContenido());
            $miPdf->Output($documento->getTitulo().'.pdf', 'I');
//            $pagina->setTitulo($documento->getTitulo());
//            $pagina->setDescripcion($documento->getDescripcion());
//
//            return $this->render(
//                    'APDIpaPropioBundle:Documentos:ver.html.pdf',array(
//                        'documento' => $documento,
//                        'pagina' => $pagina
//                        ));
        } else {
            throw new \Exception('Esta información es sólo de uso interno del Instituto Provincial del Agua!');
        }     
    }
    
    /**
     * Renderiza una página con el listado de documentos disponibles
     * 
     * @Route("/", name="ipa_documentos_listar")
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param Request $request  Utiliza knp_paginator, por eso necesita el rango de documentos a mostrar 
     * @return string Código HTML de la página con el listado de documentos
     */
    public function listarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pagina = new saquitoPaginas;
        $pagina->setTitulo('Listado de documentos');

        $documentos = $em->getRepository('APDIpaPropioBundle:ipaDocumentos')
                ->verTodos($this->get('security.authorization_checker')->isGranted('ROLE_USER'));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $documentos,
            $request->query->get('pagina', 1),
            $this->container->getParameter('proyecto')['noticias']['porpagina']
        );
        
        return $this->render('APDIpaPropioBundle:Documentos:listar.html.twig', 
                array(
                    'pagina' => $pagina,
                    'pagination' => $pagination
                ));
    }

    /**
     * Renderiza una página con el listado de documentos disponibles con un tipo
     * 
     * @Route("/tipo/{tipo}/", name="ipa_documentos_listar_tipo")
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param Request $request  Utiliza knp_paginator, por eso necesita el rango de documentos a mostrar 
     * @return string Código HTML de la página con el listado de documentos
     */
    public function listartipoAction($tipo, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pagina = new saquitoPaginas;
        $pagina->setTitulo('Listado de documentos');
        $documentos = $em->getRepository('APDIpaPropioBundle:ipaDocumentos')
                ->verTodosTipo($tipo,$this->get('security.authorization_checker')->isGranted('ROLE_USER'));
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $documentos,
            $request->query->get('pagina', 1),
            $this->container->getParameter('proyecto')['noticias']['porpagina']
        );
        
        return $this->render('APDIpaPropioBundle:Documentos:listar-tipo.html.twig', 
                array(
                    'pagina' => $pagina,
                    'tipo' => $documentos,
                    'pagination' => $pagination
                ));
    }
    
}
