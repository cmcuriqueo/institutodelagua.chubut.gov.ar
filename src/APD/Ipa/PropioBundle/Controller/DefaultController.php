<?php

namespace APD\Ipa\PropioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controlador principal del proyecto Ipa
 *
 * @category Controladores
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class DefaultController extends Controller
{

    /**
     * Armar Menu
     * 
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return string Código HTML del menu del sitio web
     */    
    public function vermenuAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $menu = $em->getRepository('APDSaquitoPaginasBundle:saquitoMenus')->buscarTodos('es');
        return $this->render('APDIpaPropioBundle:render:menu.html.twig', array(
            'menu'=> $menu
        ));        
    }    


}
