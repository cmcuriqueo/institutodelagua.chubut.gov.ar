<?php

namespace APD\Ipa\PropioBundle\Entity;

use Doctrine\ORM\EntityRepository;

use APD\Saquito\MainBundle\Entity\saquitoResultados;

/**
 * Repositorio de la entidad ipaDocumentos
 *
 * 
 * @category Repositorio
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class ipaDocumentosRepository extends EntityRepository
{

    /**
     * Buscar todos los documentos
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return array Devuelve un array de resultados ordenado por título
     */    
    public function verTodos($usuario)
    {
        if ($usuario) {
            return $this
                ->createQueryBuilder('p')
                ->select('p')
                ->orderBy('p.titulo', 'ASC')
                ->getQuery()
                ->getResult()
            ;        
        } else {
            return $this
                ->createQueryBuilder('p')
                ->select('p')
                ->where('p.privado <> true')
                ->orderBy('p.titulo', 'ASC')
                ->getQuery()
                ->getResult()
            ;        
        }
    }
    
    /**
     * Buscar todos los documentos de un tipo
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return array Devuelve un array de resultados ordenado por título
     */    
    public function verTodosTipo($tipo,$usuario)
    {
        if ($usuario) {
            return $this
                ->createQueryBuilder('p')
                ->select('p,t')
                ->leftJoin('p.tipo', 't')
                ->where('t.slug = :tipo')
                ->orderBy('p.titulo', 'ASC')
                ->setParameter('tipo', $tipo)
                ->getQuery()
                ->getResult()
            ;        
        } else {
            return $this
                ->createQueryBuilder('p')
                ->select('p,t')
                ->leftJoin('p.tipo', 't')
                ->where('p.privado <> true')
                ->andWhere('t.slug = :tipo')
                ->setParameter('tipo', $tipo)
                ->orderBy('p.titulo', 'ASC')
                ->getQuery()
                ->getResult()
            ;        
        }
    }

    /**
     * Buscar dentro de los documentos un texto en particular
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @param string $cadena Cadena a buscar en las páginas
     * @return array Devuelve un array de resultados ordenado por fecha de inicio
     */        
    public function buscar($permisos,$cadena)
    {
        $resultados = [];

        if ($permisos) {
            $qb = $this->createQueryBuilder('p')
                        ->orderBy('p.titulo', 'DESC')
                        ->setMaxResults( 30 )
                        ->where('p.titulo LIKE :cadena')
                        ->orWhere('p.descripcion LIKE :cadena')
                        ->orWhere('p.contenido LIKE :cadena')
                        ->setParameter('cadena', '%' . $cadena . '%')
                    ;
        } else {
            $qb = $this->createQueryBuilder('p')
                        ->orderBy('p.titulo', 'DESC')
                        ->setMaxResults( 30 )
                        ->where('p.titulo LIKE :cadena')
                        ->orWhere('p.descripcion LIKE :cadena')
                        ->orWhere('p.contenido LIKE :cadena')
                        ->andWhere('p.privado <> true')
                        ->setParameter('cadena', '%' . $cadena . '%')
                    ;
        }

        $query = $qb->getQuery();
        try {
            $lista = $query->getResult();
            foreach ($lista as $val) {
                $item = new saquitoResultados();
                $item->setTitulo($val->getTitulo());
                if ($val->getPrivado()) {
                    $item->setDescripcion('<strong>[DOCUMENTOS PRIVADOS] </strong> ' . $val->getTipo() );
                } else {
                    $item->setDescripcion('<strong>[DOCUMENTOS] </strong> ' . $val->getTipo());
                }
                
//                $item->setFecha(new \DateTime());
                $item->setEntidad("ipaDocumentos");
                $item->setLink("/documentos/". $val->getTipo()->getSlug() ."/". $val->getId() ."/".$val->getSlug());
                array_push($resultados, $item);
            }
            return $resultados;
        } catch (\Doctrine\ORM\NoResultException $e){
            return null;
        }
    }          
    
}
