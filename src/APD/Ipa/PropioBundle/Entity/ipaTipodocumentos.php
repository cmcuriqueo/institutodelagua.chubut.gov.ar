<?php

namespace APD\Ipa\PropioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entidad encargada de el manejo de los 'Tipo de documentos del IPA 
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks 
 * @ORM\Entity
 * 
 * @category Entidades
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */

class ipaTipodocumentos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="APD\Ipa\PropioBundle\Entity\ipaDocumentos", mappedBy="tipo")
     */    
    private $documentos;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ipaTipodocumentos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ipaTipodocumentos
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function __construct() {
        $this->documentos = new ArrayCollection();   
    }

    /**
     * Get documentos
     *
     * @return APD\Ipa\PropioBundle\Entity\ipaDocumentos
     */
    public function getDocumentos()
    {
        return $this->documentos;
    }    
    
    public function __toString() {
        return $this->descripcion;
    }    
}
