<?php
namespace APD\Ipa\PropioBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clase para administrar los documentos (SonataAdmin)
 *
 * 
 * @category Admin
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class DocumentosAdmin extends Admin
{
//    public $supportsPreviewMode = true;
    protected $baseRouteName = 'ipa_documentos_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'id' // field name
        );    
   
    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    private $container;

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $sizes = array();
        for ($index = 35; $index <= 48; $index++) {
            $sizes[$index] = $index . ""; 
        }
        

        $formMapper
            ->add('numero',null, 
                array(
                    'label' => 'Número: ',
                    'required'  => false
                ))
            ->add('titulo',null, 
                array(
                    'label' => 'Título: '
                ))
            ->add('tipo')   
            ->add('fecha', 'sonata_type_date_picker', array(
                    'format'                => 'dd/M/yyyy',
                    'dp_side_by_side'       => true,
            ))                
            ->add('privado',null, 
                array(
                    'label' => 'USO INTERNO: '
                ))
            ->add('descripcion',null, 
                array(
                    'label' => 'Descripción: ',
                    'required'  => false
                ))
            ->add('contenido', 'ckeditor', array(
                'config' => array(
                    'filebrowser_image_browse_url' => array(
                        'route'            => 'elfinder',
                        'route_parameters' => array('instance' => 'ckeditor'),
                        ),
                    ),
                ))                
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('tipo')
            ->add('privado', null,
                    array( 'label' => 'Uso Interno'
                    ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titulo')
            ->add('fecha')
            ->add('tipo')      
            ->add('privado', null,
                    array( 'label' => 'Uso Interno'
                    ))        ;
    }

    public function prePersist($entidad) {
        $entidad->setSlug($this->container->get('utilesTextos')->slugify($entidad->getTitulo()));
    }

    public function preUpdate($entidad) {
        $entidad->setSlug($this->container->get('utilesTextos')->slugify($entidad->getTitulo()));
    }

    public function postPersist($entidad) {
    }

    public function postUpdate($entidad) {
    }

    public function subirArchivo($entidad) {
    }

    protected function configureRoutes(RouteCollection $collection) 
    {

    }    
}