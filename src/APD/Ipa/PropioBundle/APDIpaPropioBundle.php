<?php

namespace APD\Ipa\PropioBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Paquete propio del proyecto para la Web del IPA Chubut
 *
 * 
 * @category Bundle
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDIpaPropioBundle extends Bundle
{
}
