<?php

namespace APD\Ipa\MiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controlador principal del proyecto Ipa
 *
 * @category Controladores
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class DefaultController extends Controller
{   
    public function vermenuAction()
    {

        return $this->render('APDIpaMiBundle:template:template.html.twig');
        
    }    


}
