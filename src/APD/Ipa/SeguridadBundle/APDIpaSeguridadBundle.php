<?php

namespace APD\Ipa\SeguridadBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Extiende el paquete APDSaquitoSeguridadBundle
 *
 * 
 * @category Bundle
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDIpaSeguridadBundle extends Bundle
{
    public function getParent()
    {
        return 'APDSaquitoSeguridadBundle';
    }
}
