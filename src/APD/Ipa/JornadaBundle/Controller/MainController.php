<?php

namespace APD\Ipa\JornadaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;


class MainController extends Controller
{   
    public function verGaleriaAction()
    {

        return $this->render('APDIpaJornadaBundle:template:galeria.html.twig');
        
    }    


}