<?php

namespace APD\Ipa\MainBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Extiende el paquete APDSaquitoMainBundle
 *
 * 
 * @category Bundle
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDIpaMainBundle extends Bundle
{
    public function getParent()
    {
        return 'APDSaquitoMainBundle';
    }
}
