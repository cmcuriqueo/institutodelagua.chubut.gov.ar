<?php

namespace APD\Ipa\PaginasBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Extiende el paquete APDSaquitoPaginasBundle
 *
 * 
 * @category Bundle
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDIpaPaginasBundle extends Bundle
{
    public function getParent()
    {
        return 'APDSaquitoPaginasBundle';
    }
}
