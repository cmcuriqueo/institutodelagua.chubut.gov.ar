<?php

namespace APD\Ipa\NoticiasBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Extiende el paquete APDSaquitoNoticiasBundle
 *
 * 
 * @category Bundle
 * @package Ipa
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDIpaNoticiasBundle extends Bundle
{
    public function getParent()
    {
        return 'APDSaquitoNoticiasBundle';
    }
}
