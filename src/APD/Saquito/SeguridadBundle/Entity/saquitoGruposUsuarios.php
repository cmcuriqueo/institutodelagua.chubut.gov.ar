<?php
// src/APD/Saquito/SeguridadBundle/Entity/saquitoUsuarios.php
namespace APD\Saquito\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseGroup as BaseGroup;

/**
 * Entidad encargada de el manejo de los 'Grupos de Usuario'
 *
 * @ORM\Entity
 * @ORM\Table(name="saquitoGruposUsuarios")
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoGruposUsuarios extends BaseGroup
{
    /**
     * @var integer $id
     */
    protected $id;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }
}