<?php

namespace APD\Saquito\UsuariosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controlador por defecto de los fragmentos
 *
 * @Route("/usuarios")
 * 
 * @category Controladores
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */

class UsuariosController extends Controller
{

    /**
     * Renderiza una página con el login 
     * 
     * @Route("/ingresar", name="saquito_usuarios_ingresar")
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     */
    public function ingresarAction($slug)
    {
        $fragmento = $this->getDoctrine()
            ->getRepository('APDSaquitoPaginasBundle:saquitoFragmentos')
            ->findOneByNombre($slug);

        if (!$fragmento) {
            return new Response('<div style="padding:10px;">{Necesitás crear un <i><strong>fragmento</strong></i> con el nombre <b>"'. $slug . '"</b> para que aparezca aquí.}</div>');
        } else {
            return new Response($fragmento->getContenido());
        }
    }
    
}
