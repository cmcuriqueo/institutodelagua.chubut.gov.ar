<?php

namespace APD\Saquito\SeguridadBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Paquete encargado de la seguridad del proyecto Saquito
 *
 * 
 * @category Bundle
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDSaquitoSeguridadBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataUserBundle';
    }
}
