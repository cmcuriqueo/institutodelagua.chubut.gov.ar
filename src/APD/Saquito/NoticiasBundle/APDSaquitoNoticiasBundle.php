<?php

namespace APD\Saquito\NoticiasBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Paquete encargado de la administración de Artículos de novedades
 *
 * 
 * @category Bundle
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDSaquitoNoticiasBundle extends Bundle
{
}
