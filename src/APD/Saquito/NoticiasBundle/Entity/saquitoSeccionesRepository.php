<?php

namespace APD\Saquito\NoticiasBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Repositorio de la entidad saquitoSecciones
 *
 * 
 * @category Repositorio
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoSeccionesRepository extends EntityRepository
{
}
