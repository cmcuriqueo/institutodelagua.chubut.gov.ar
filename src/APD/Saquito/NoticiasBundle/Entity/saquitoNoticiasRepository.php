<?php

namespace APD\Saquito\NoticiasBundle\Entity;

use Doctrine\ORM\EntityRepository;

use APD\Saquito\MainBundle\Entity\saquitoResultados;

/**
 * Repositorio de la entidad saquitoNoticias
 *
 * 
 * @category Repositorio
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoNoticiasRepository extends EntityRepository
{
    /**
     * Buscar las últimas 10 noticias
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return array Devuelve un array de resultados ordenado por fecha
     */     
    public function recuperarUltimas($idioma,$usuario)
    {
        if ($usuario) {
            return $this
                ->createQueryBuilder('p')
                ->select('p')
                ->where('p.fecha <= :now')->setParameter('now', new \DateTime())
                ->andWhere('p.idioma = :idioma')->setParameter('idioma', $idioma)
                ->orderBy('p.fecha', 'DESC')
                ->setMaxResults(20)
                ->getQuery()
                ->getResult()
            ;        
        } else {
            return $this
                ->createQueryBuilder('p')
                ->select('p')
                ->where('p.fecha <= :now')->setParameter('now', new \DateTime())
                ->andWhere('p.idioma = :idioma')->setParameter('idioma', $idioma)
                ->andWhere('p.privado <> true')
                ->orderBy('p.fecha', 'DESC')
                ->setMaxResults(20)
                ->getQuery()
                ->getResult()
            ;        
        } 
    }

    /**
     * Buscar todas las noticias
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return array Devuelve un array de resultados ordenado por fecha
     */    
    public function recuperarTodos($usuario)
    {

        if ($usuario) {
            return $this
                ->createQueryBuilder('p')
                ->select('p')
                ->where('p.fecha <= :now')->setParameter('now', new \DateTime())
                ->orderBy('p.fecha', 'DESC')
                ->getQuery()
                ->getResult()
            ;        
        } else {
            return $this
                ->createQueryBuilder('p')
                ->select('p')
                ->where('p.fecha <= :now')->setParameter('now', new \DateTime())
                ->andWhere('p.privado <> true')
                ->orderBy('p.fecha', 'DESC')
                ->getQuery()
                ->getResult()
            ;        

        }
            
    }
    
    /**
     * Buscar dentro de las noticias un texto en particular
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @param string $cadena Cadena a buscar en las noticias
     * @param boolean $permisos Usuario logeado con permisos
     * @return array Devuelve un array de resultados ordenado por fecha (máximo 10 noticias)
     */        
    public function buscar($permisos,$cadena)
    {
        $resultados = [];
         
        $qb = $this->createQueryBuilder('c')
                    ->orderBy('c.fecha', 'DESC')
                    ->setMaxResults( 30 )
                    ->where('c.copete LIKE :cadena')
                    ->orWhere('c.titulo LIKE :cadena')
                    ->orWhere('c.volanta LIKE :cadena')
                    ->orWhere('c.cuerpo LIKE :cadena')
                    ->setParameter('cadena', '%' . $cadena . '%')
                ;

        $query = $qb->getQuery();
        try {
            $lista = $query->getResult();
            foreach ($lista as $val) {
                $item = new saquitoResultados();
                $item->setTitulo($val->getTitulo());
                $item->setDescripcion("<strong>[ARTÍCULOS] </strong> "  . $val->getVolanta());
                $item->setFecha($val->getFecha());
                $item->setEntidad("saquitoNoticias");
                $item->setLink("/articulos/es/articulo/" . $val->getId() . "/" . $val->getSlug());
                array_push($resultados, $item);
            }
            return $resultados;
        } catch (\Doctrine\ORM\NoResultException $e){
            return null;
        }
    }        
}
