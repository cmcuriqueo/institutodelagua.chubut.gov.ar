<?php

namespace APD\Saquito\NoticiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entidad encargada de el manejo de los 'Secciones' a las que puede pertenecer una noticia
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks 
 * @ORM\Entity(repositoryClass="APD\Saquito\NoticiasBundle\Entity\saquitoSeccionesRepository")
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoSecciones
{
    /**
     * @var integer
     * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     */
    private $descripcion;

    /**
     * @ORM\ManyToMany(targetEntity="APD\Saquito\NoticiasBundle\Entity\saquitoNoticias", mappedBy="secciones")
     * @ORM\OrderBy({"titulo" = "ASC"})
     */
    private $noticias;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;
    
    /**
     * Get noticias
     *
     */
    public function getNoticias()
    {
        return $this->noticias;
    }

    public function __construct() {

        $this->noticias = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return saquitoSecciones
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
    
    public function __toString() {
        return $this->descripcion;
    }
    
}
