<?php

namespace APD\Saquito\NoticiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entidad encargada de el manejo de los 'Noticias'
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="APD\Saquito\NoticiasBundle\Entity\saquitoNoticiasRepository")
 *
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoNoticias
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $copete;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\NotBlank(message="Tenés que ingresar un título")
     */
    private $titulo;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\NotBlank(message="Tenés que ingresar una volanta")
     */
    private $volanta;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *     min = "10",
     *     minMessage = "El límite mínimo es de ({{ limit }} caracteres)"
     * )
     */
    private $cuerpo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $fotoportada;

    /**
     * @ORM\Column(type="boolean")
     */
    private $portada;

    /**
     * @ORM\Column(type="boolean")
     */
    private $destacada;

    /**
     * @var boolean
     *
     * @ORM\Column(name="privado", type="boolean", nullable=true)
     */
    private $privado;

    /**
     * @var string $idioma
     *
     * @ORM\Column(name="idioma", type="string", length=10, nullable=true, options={"default": "es"})
     */
    private $idioma;

    /**
     * @ORM\ManyToMany(targetEntity="APD\Saquito\NoticiasBundle\Entity\saquitoSecciones", inversedBy="noticias")
     * @ORM\JoinTable(name="noticias_secciones",
     *      joinColumns={@ORM\JoinColumn(name="noticias_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="secciones_id", referencedColumnName="id")}
     *      )
     */
    private $secciones;

    /**
     * @ORM\Column(name="visualizaciones", type="integer")
     */
    private $visualizaciones;

    /**
     * Get secciones
     *
     * @return APD\Saquito\NoticiasBundle\Entity\saquitoSecciones
     */
    public function getSecciones()
    {
        return $this->secciones;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }


    public function getFecha()
    {
        return $this->fecha;
    }

    public function setPortada($portada)
    {
        $this->portada = $portada;

        return $this;
    }

    public function getPortada()
    {
        return $this->portada;
    }

    public function setFotoportada($fotoportada)
    {
        $this->fotoportada = $fotoportada;

        return $this;
    }

    public function getFotoportada()
    {
        return $this->fotoportada;
    }

    public function setDestacada($destacada)
    {
        $this->destacada = $destacada;

        return $this;
    }

    public function getDestacada()
    {
        return $this->destacada;
    }

    public function setCopete($copete)
    {
        $this->copete = $copete;

        return $this;
    }

    public function getCopete()
    {
        return $this->copete;
    }

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    public function getVisualizaciones()
    {
        return $this->visualizaciones;
    }

    public function setVisualizaciones($visualizaciones)
    {
        $this->visualizaciones = $visualizaciones;

        return $this;
    }

    public function setVolanta($volanta)
    {
        $this->volanta = $volanta;

        return $this;
    }

    public function getVolanta()
    {
        return $this->volanta;
    }

    public function setCuerpo($cuerpo)
    {
        $this->cuerpo = $cuerpo;

        return $this;
    }

    public function getCuerpo()
    {
        return $this->cuerpo;
    }

    /**
     * Set idioma
     *
     * @param string $idioma
     * @return saquitoNoticias
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;
        return $this;
    }

    /**
     * Get idioma
     *
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set privado
     *
     * @param boolean $privado
     * @return saquitoNoticias
     */
    public function setPrivado($privado)
    {
        $this->privado = $privado;

        return $this;
    }

    /**
     * Get privado
     *
     * @return boolean
     */
    public function getPrivado()
    {
        return $this->privado;
    }

//    public function setSeccion(\APD\Saquito\NoticiasBundle\Entity\saquitoSecciones $seccion = null)
//    {
//        $this->seccion = $seccion;
//        return $this;
//    }
//
//    public function getSeccion()
//    {
//        return $this->seccion;
//    }

    public function __construct() {
        $this->destacada = false;
        $this->portada = false;
        $this->setPrivado(false);
        $this->fotoportada = false;
        $this->secciones = new ArrayCollection();

    }

    public function __toString()
    {
      return $this->titulo;
    }

}
