<?php

// src/APD/Saquito/NoticiasBundle/DataFixtures/ORM/LoadInicial.php

namespace APD\Saquito\NoticiasBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use APD\Saquito\NoticiasBundle\Entity\saquitoSecciones;
use APD\Saquito\NoticiasBundle\Entity\saquitoNoticias;
use APD\Saquito\PaginasBundle\Entity\saquitoLinks;

/**
 * Carga los datos de prueba de Noticias
 *
 * 
 * @category Datafixtures
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class LoadInicial extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder()
    {
        return 4;
    }

    public function load(ObjectManager $manager)
    {

        $yaml = new Parser();
        
        $proyecto = $this->container->getParameter('proyecto')['nombre'];

        $collecion = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/NoticiasBundle/secciones.yml'));
        foreach ($collecion['secciones'] as $datos) {
            $seccion = new saquitoSecciones;
            $seccion->setDescripcion($datos['descripcion']);
            $seccion->setSlug($this->container->get('utilesTextos')->slugify($seccion->getDescripcion()));

            $manager->persist($seccion);
            
            $this->addReference('seccion-'.$datos['descripcion'], $seccion);
        }
        
        // Creo un link para el listado de todas las secciones (oculto en el menú)
        foreach ($collecion['secciones'] as $datos) {
            $link = new saquitoLinks;

            $link->setTitulo($datos['descripcion']);
            $link->setSlug($this->container->get('utilesTextos')->slugify($link->getTitulo()));
            $link->setUrl($this->container->get('router')->generate('saquito_noticias_listar_seccion',array('_locale' => 'es', 'seccion' => $link->getSlug())));

            $manager->persist($link);
        }
        
        
        // Cargar datos de las Noticias dinamicas
        $collecion2 = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/NoticiasBundle/noticias.yml'));
        foreach ($collecion2['noticias'] as $datos) {
            $articulo = new saquitoNoticias;
            if (!empty($datos['titulo'])) {
                $articulo->setTitulo($datos['titulo']);
            } else {
                $articulo->setTitulo("Título de la noticia");
            }
            $articulo->setSlug($this->container->get('utilesTextos')->slugify($articulo->getTitulo()));

            if (!empty($datos['idioma'])) {
                $articulo->setIdioma($datos['idioma']);
            } else {
                $articulo->setIdioma($this->container->getParameter('proyecto')['idiomapordefecto']);
            }
            
            if (!empty($datos['volanta'])) {
                $articulo->setVolanta($datos['volanta']);
            }
            if (!empty($datos['copete'])) {
                $articulo->setCopete($datos['copete']);
            }
            if (!empty($datos['fotoportada'])) {
                $articulo->setFotoportada($datos['fotoportada']);
            } 
            if (!empty($datos['portada'])) {
                $articulo->setPortada($datos['portada']);
            }
            if (!empty($datos['destacada'])) {
                $articulo->setDestacada($datos['destacada']);
            }
            if (!empty($datos['fecha'])) {
                $articulo->setFecha(new \DateTime($datos['fecha']));
            } else {
                $articulo->setFecha(new \DateTime("01/01/2100"));
            }
            if (!empty($datos['cuerpo'])) {
                $articulo->setCuerpo($datos['cuerpo']);
            } else {
                $articulo->setCuerpo("Debe agregar un contenido al cuerpo del artículo");
            }
            
            if (!empty($datos['seccion'])) {
                $articulo->getSecciones()->add($manager->merge($this->getReference('seccion-'. utf8_encode($datos['seccion']))));
            }
            
            $manager->persist($articulo);
        }

        $manager->flush();
    }

}