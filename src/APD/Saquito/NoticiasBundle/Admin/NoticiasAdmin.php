<?php
namespace APD\Saquito\NoticiasBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Clase para administrar los artículos de las noticias (SonataAdmin)
 *
 *
 * @category Admin
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class NoticiasAdmin extends Admin
{
    protected $baseRouteName = 'saquitonoticias_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'fecha' // field name
        );

    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    private $container;

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Información del artículo',array('collapsed'=>true))
                ->add('idioma','choice',
                    array(
                        'choices' => $this->container->getParameter('proyecto')['idiomas'],
                        'attr' => array('style' => 'width:200px')
                    ))
                ->add('titulo', null,
                    array(
                    ))
                ->add('volanta', null,
                    array(
                    ))
                ->add('privado',null,
                    array(
                        'label' => 'USO INTERNO: '
                    ))
                ->add('portada',null,
                    array(
                        'label' => 'Mostrar en portada: ',
                        'required'  => false
                    ))
                ->add('destacada',null,
                    array(
                        'label' => 'Mostrar en portada como destacada: ',
                        'required'  => false
                    ))
                ->add('visualizaciones',null,
                    array(
                        'label' => 'Visualizaciones del articulo',
                        'required'  => false,
                        'attr'=> array('style'=>'width:225px', 'type' => 'numeric', 'min' => 0)
                    ))
                ->add('fecha', 'sonata_type_datetime_picker', array(
                        'format'                => 'dd/M/yyyy HH:mm',
                        'dp_side_by_side'       => true,
                        'dp_use_current'        => false,
                        'dp_use_seconds'        => false
                ))

//                ->add('fecha', 'sonata_type_datetime_picker', array('label'=>'Fecha y hora:'))
                ->add('copete','textarea',array('attr'=>array('style'=>'height:100px;')))
                ->add('cuerpo', 'ckeditor', array(
                    'config' => array(
                        'filebrowser_image_browse_url' => array(
                            'route'            => 'elfinder',
                            'route_parameters' => array('instance' => 'ckeditor'),
                            ),
                        ),
                    ))
            ->end()
            ->with('Secciones a la que pertenece el artículo',array('collapsed'=>true))
                ->add('secciones', 'entity', array(
                    'multiple' => true,
                    'expanded' => true,
                    'property' => 'descripcion',
                    'class'    => 'APDSaquitoNoticiasBundle:saquitoSecciones',
                    'label'    => ' ',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                            ->OrderBy('p.descripcion', 'ASC');
                    }
                ))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('fecha')
            ->add('idioma')
            ->add('privado', null,
                    array( 'label' => 'Uso Interno'
                    ))
            ->add('portada')
            ->add('destacada')
            ->add('titulo')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titulo')
            ->add('portada')
            ->add('destacada')
            ->add('idioma')
            ->add('fecha')
            ->add('privado', null,
                    array( 'label' => 'Uso Interno'
                    ))
            ->add('_action', 'Acciones', array(
                    'actions' => array(
//                    'edit' => array(),
//                    'delete' => array('label' =>''),
                    'fotoportada' => array('template' => 'APDSaquitoNoticiasBundle:Admin:list__action_fotoportada.html.twig'),
                    'publicar' => array('template' => 'APDSaquitoNoticiasBundle:Admin:list__action_publicar.html.twig'),
            )));
    }

//    public function validate(ErrorElement $errorElement, $object)
//    {
//        $errorElement
//            ->with('titulo')
//                ->assertLength(array('max' => 150))
//            ->end()
//        ;
//    }

//    public function validate(ErrorElement $errorElement, $object)
//    {
//        $errorElement
//            ->with('titulo')
//                ->assertMaxLength(array('limit' => 150))
//            ->end()
//            ->with('copete')
//                ->assertMaxLength(array('limit' => 255))
//            ->end()
//            ->with('volanta')
//                ->assertMaxLength(array('limit' => 50))
//            ->end()
//        ;
//    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setVisualizaciones(0);

        return $instance;
    }

    public function prePersist($noticia) {
        $noticia->setSlug($this->container->get('utilesTextos')->slugify($noticia->getTitulo()));
    }

    public function preUpdate($noticia) {
        $noticia->setSlug($this->container->get('utilesTextos')->slugify($noticia->getTitulo()));
    }

    public function postRemove($entidad) {
//        $this->controlCambios($entidad,'Baja');
    }

    public function postUpdate($entidad) {
//        $this->controlCambios($entidad,'Modificación');
    }

    public function postPersist($entidad) {
//        $this->controlCambios($entidad,'Alta');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('eliminarfoto',$this->getRouterIdParameter().'/eliminarfoto');
        $collection->add('subirfoto',$this->getRouterIdParameter().'/subirfoto');
        $collection->add('fotoportada',$this->getRouterIdParameter().'/fotoportada');
        $collection->add('publicado',$this->getRouterIdParameter().'/publicado');
        $collection->add('publicar',$this->getRouterIdParameter().'/publicar');
    }
}
