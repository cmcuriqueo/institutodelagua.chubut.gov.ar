<?php

//APD\Saquito\NoticiasBundle\Controller\NoticiasAdminController.php

namespace APD\Saquito\NoticiasBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

/**
 * Acciones adicionales para las noticias en SonataAdmin
 *
 * 
 * @category AdminCoroller
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class NoticiasAdminController extends Controller
{
    /**
     * Función de ingreso a publicar un artículo en las redes sociales del proyecto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param integer $id Id del Artículo a publicar
     * @return string Renderiza la página con el aviso para publicar el artículo y el pedido de hashtdag
     */
    public function publicarAction($id = null){
        
        $repository = $this->getDoctrine()->getRepository('APDSaquitoNoticiasBundle:saquitoNoticias');
        $noticia = $repository->find($id);
        $config = $this->container->getParameter('proyecto');
        $ruta = $config['sitename'] . '/' . $config['nombre']. '/hot/noticias/';
        if ($noticia->getFotoportada() == 1) {
            $foto = $ruta. $noticia->getId() .'.jpg';
        } else {
            $foto = $ruta . 'vacio.jpg';
        }

        return $this->render(
            'APDSaquitoNoticiasBundle:Admin:publicar.html.twig', array(
                'noticia' => $noticia,
                )
        );   
    }   

    /**
     * Publica el artículo en las redes sociales del proyecto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     *  
     * @category function
     * @param integer $id Id del Artículo a publicar
     * @return string Renderiza la página con el resultado si publicó o no el artículo
     */
    public function publicadoAction($id = null){

        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {
            
            if ($request->get("hashtag") <> null) {
                $hashtag = "#" . $request->get("hashtag") . " | ";
            } else {
                $hashtag = "";
            }
            
            $repository = $this->getDoctrine()->getRepository('APDSaquitoNoticiasBundle:saquitoNoticias');
            $noticia = $repository->find($id);
            $config = $this->container->getParameter('proyecto');
            $ruta = $config['sitename'] . '/' . $config['nombre']. '/hot/noticias/';
            if ($noticia->getFotoportada() == 1) {
                $foto = $ruta. $noticia->getId() .'.jpg';
            } else {
                $foto = $ruta . 'vacio.jpg';
            }
            
    //        Publicar en cuenta de Facebook
            $facebookeado = $this->container->get('saquitoFacebook')->publicarMensaje(
                $hashtag . $noticia->getTitulo(), 
                substr($noticia->getCopete(), 0, 210). '... - [ver artículo completo]', 
                $config['sitename'] . $this->get('router')->generate('saquito_noticias_ver', array(
                    'id' => $noticia->getId(),
                    'slug' => $noticia->getSlug())
                    ),
                $noticia->getVolanta(), 
                $foto
            );
    //        Publicar en cuenta de Twitter
            $mensaje = $hashtag . substr($noticia->getTitulo(), 0, 100) . '... - ver artículo completo en ' . $config['sitename']; 
            $twiteado = $this->container->get('saquitoTwitter')->publicarMensaje($mensaje);
            $facebookeado = true;
            $twiteado = true;
            return $this->render(
                'APDSaquitoNoticiasBundle:Admin:publicado.html.twig', array(
                    'noticia' => $noticia,
                    'facebookeado' => $facebookeado,
                    'twiteado' => $twiteado
                    )
            );   
        } else {
            
        }
    }   
    
    
    /**
     * Renderiza el formulario para editar la foto de portada del artículo
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param integer $id Id del Artículo a editar la portada
     * @return string Código HTML del formulario de edición de la foto de portada
     */
    public function fotoportadaAction($id = null){
        $repository = $this->getDoctrine()->getRepository('APDSaquitoNoticiasBundle:saquitoNoticias');
        $noticia = $repository->find($id);
        
        return $this->render(
            'APDSaquitoNoticiasBundle:Admin:editar_foto_portada.html.twig', array('noticia' => $noticia)
        );   
    }   
    
    /**
     * Sube el archivo de la foto de portada al servidor
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param integer $id Id del artículo 
     * @return string Código HTML del formulario de edición de la foto de portada
     */
    public function subirfotoAction($id = null){
        $request = $this->getRequest();

        $config = $this->container->getParameter('proyecto');
        $repository = $this->getDoctrine()->getRepository('APDSaquitoNoticiasBundle:saquitoNoticias');
        $noticia = $repository->find($id);
        
        if ($request->getMethod() == 'POST') {
            //$archivo = $this->container->getParameter('proyecto')['nombre'] . '\hot\noticias\\' . $id . '.jpg';
            $archivo = __DIR__ . '/../../../../../' . $config['carpeta_publica'] . '/' . $config['nombre'] . '/hot/noticias/'. $id . '.jpg';
            
            if ( \move_uploaded_file($_FILES['foto']['tmp_name'], $archivo)){
                $em = $this->getDoctrine()->getEntityManager();
                $repository = $this->getDoctrine()->getRepository('APDSaquitoNoticiasBundle:saquitoNoticias');
                $noticia = $repository->find($id);
                $noticia->setFotoportada(1);
                $em->persist($noticia);
                $em->flush();
            } else {
                $errors= error_get_last();
                echo "COPY ERROR: ".$errors['type'];
                echo "<br />\n".$errors['message'];                 
            }
        }
        return $this->render(
            'APDSaquitoNoticiasBundle:Admin:editar_foto_portada.html.twig', array('noticia' => $noticia)
        );   
    }   
    
    /**
     * Deja sin foto de portada a el Artículo indicado 
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param integer $id Id del artículo
     * @return string Código HTML del formulario de edición de la foto de portada
     */
    public function eliminarfotoAction($id = null){
        $em = $this->getDoctrine()->getEntityManager();
        $repository = $this->getDoctrine()->getRepository('APDSaquitoNoticiasBundle:saquitoNoticias');
        $noticia = $repository->find($id);
        $noticia->setFotoportada(0);
        $em->persist($noticia);
        $em->flush();
        
        return $this->render(
            'APDSaquitoNoticiasBundle:Admin:editar_foto_portada.html.twig', array('noticia' => $noticia)
        );   
    }   

}