<?php

namespace APD\Saquito\NoticiasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use APD\Saquito\NoticiasBundle\Entity\saquitoNoticias;
use APD\Saquito\PaginasBundle\Entity\saquitoPaginas;

/**
 * Controlador principal del Bundle Noticias
 *
 * @Route("/articulos")
 *
 * @category Controladores
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class NoticiasController extends Controller
{

    /**
     * Renderiza una página con la noticia seleccionada
     *
     * @Route("/{_locale}/articulo/{id}/{slug}", name="saquito_noticias_ver")
     *
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     *
     * @category function
     * @param saquitiNoticias $articulo Noticia a mostrar
     * @return string Código HTML de la página con la noticia
     */
    public function verArticuloAction(saquitoNoticias $articulo)
    {
        $pagina = new saquitoPaginas;

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') or ($articulo->getPrivado() == false)) {
            $pagina->setTitulo($articulo->getTitulo());
            $pagina->setDescripcion($articulo->getCopete());
            if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER'))
            {
                $em = $this->getDoctrine()->getManager();
                $views = $articulo->getVisualizaciones();
                $articulo->setVisualizaciones($views+1);
                // tells Doctrine you want to (eventually) save the articulo (no queries yet)
                $em->persist($articulo);

                // actually executes the queries (i.e. the INSERT query)
                $em->flush();
            }

            return $this->render(
                    'APDSaquitoNoticiasBundle:Noticias:ver.html.twig',array(
                        'articulo' => $articulo,
                        'pagina' => $pagina
                        ));
        } else {
            throw new \Exception('Esta información tiene acceso restringido.');
        }

    }

    /**
     * Renderiza una página con el listado de noticias disponibles
     *
     * @Route("/{_locale}/", name="saquito_noticias_listar")
     *
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     *
     * @category function
     * @param Request $request  Utiliza knp_paginator, por eso necesita el rango de noticias a mostrar
     * @return string Código HTML de la página con el listado de noticias
     */
    public function listarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pagina = new saquitoPaginas;
        $pagina->setTitulo('Listado de noticias');
        $noticias = $em->getRepository('APDSaquitoNoticiasBundle:saquitoNoticias')
                ->recuperarTodos($this->get('security.authorization_checker')->isGranted('ROLE_USER'));
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $noticias,
            $request->query->get('pagina', 1),
            $this->container->getParameter('proyecto')['noticias']['porpagina']
        );

        return $this->render('APDSaquitoNoticiasBundle:Noticias:listar.html.twig',
                array(
                    'pagina' => $pagina,
                    'pagination' => $pagination
                ));
    }

    /**
     * Renderiza una página con el listado de noticias de una seccion en particular
     *
     * @Route("/{_locale}/seccion/{seccion}/", name="saquito_noticias_listar_seccion")
     *
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     *
     * @category function
     * @param Request $request  Utiliza knp_paginator, por eso necesita el rango de noticias a mostrar
     * @return string Código HTML de la página con el listado de noticias
     */
    public function listarSeccionAction($seccion, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pagina = new saquitoPaginas;
        $pagina->setTitulo('Listado de noticias');
        $miSeccion = $em->getRepository('APDSaquitoNoticiasBundle:saquitoSecciones')->findOneBySlug($seccion);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $miSeccion->getNoticias(),
            $request->query->get('pagina', 1),
            $this->container->getParameter('proyecto')['noticias']['porpagina']
        );

        return $this->render('APDSaquitoNoticiasBundle:Noticias:listar-seccion.html.twig',
                array(
                    'pagina' => $pagina,
                    'seccion' => $miSeccion,
                    'pagination' => $pagination
                ));
    }

}
