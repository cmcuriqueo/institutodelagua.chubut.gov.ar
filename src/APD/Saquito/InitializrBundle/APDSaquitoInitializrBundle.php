<?php

namespace APD\Saquito\InitializrBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Paquete encargado de la integración de Initializr al proyecto Saquito
 *
 * 
 * @category Bundle
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDSaquitoInitializrBundle extends Bundle
{
}
