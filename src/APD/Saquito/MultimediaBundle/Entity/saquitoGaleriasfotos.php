<?php

namespace APD\Saquito\MultimediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Entidad encargada de el manejo de las 'Fotos de las Galerías' 
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoGaleriasfotos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="APD\Saquito\MultimediaBundle\Entity\saquitoGalerias", inversedBy="fotos", cascade={"persist"})
     * @ORM\JoinColumn(name="galeria_id", referencedColumnName = "id")
     */
    protected $galeria;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return saquitoGaleriasfotos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    /**
     * Set galeria
     *
     * @param APD\Saquito\MultimediaBundle\Entity\saquitoGalerias $galeria
     * @return saquitoGaleriasfotos
     */
    public function setGaleria(\APD\Saquito\MultimediaBundle\Entity\saquitoGalerias $galeria = null)
    {
        $this->galeria = $galeria;
        return $this;
    }

    /**
     * Get galeria
     *
     * @return APD\Saquito\MultimediaBundle\Entity\saquitoGalerias
     */
    public function getGaleria()
    {
        return $this->galeria;
    }            

    /**
     * Set orden
     *
     * @param integer $orden
     * @return saquitoGaleriasfotos
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -     
  
    /**
     * @Assert\File(
     *     maxSize = "15M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "Los archivos no pueden pesar más de 15Mb.",
     *     mimeTypesMessage = "Sólo se permiten archivos de imagen."
     * )
     */
    private $archivofoto;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;
    
    /**
     * Set path
     *
     * @param string $path
     * @return saquitoLibros
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }    
    
    /**
     * Set archivofoto
     *
     * @param UploadedFile $archivo
     */
    public function setArchivofoto(UploadedFile $archivo = null)
    {
        $this->archivofoto = $archivo;
    }

    /**
     * Get archivofoto
     *
     * @return UploadedFile
     */
    public function getArchivofoto()
    {
        return $this->archivofoto;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../../' . $this->getPath() . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return '/hot/galerias/fotos';
    }

    public function getVerfoto() {
//         Le saco la carpeta pública (public_html, web, o lo que sea...) a path, en este caso no tiene que estar.
        return null === $this->getPath()
            ? null
            : substr($this->getPath(), strpos($this->getPath(), '/')) . $this->getUploadDir() . '/' . $this->getId() . '.jpg';        
    }
    
    /**
     * @ORM\PreRemove()
     */
    public function removeUpload()
    {
        $file = $this->getUploadRootDir().'/' . $this->getId().'.jpg';
        if (file_exists($file)) {
            unlink($file);
        }
    }

    public function upload()
    {
        if (null === $this->archivofoto) {
            return;
        }
        $this->archivofoto->move( $this->getUploadRootDir(),  $this->getId().'.jpg');
        $this->archivofoto = null;
    }
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
    public function __toString()
    {
      return $this->orden . ' - ' . $this->descripcion;
    }    
        
    
}
