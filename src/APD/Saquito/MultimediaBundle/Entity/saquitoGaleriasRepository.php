<?php

namespace APD\Saquito\MultimediaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Repositorio de la entidad saquitoGalerias
 *
 * 
 * @category Repositorio
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoGaleriasRepository extends EntityRepository
{
    /**
     * listar las galerias activas
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return array Devuelve la página o null si no lo encuentra
     */      
    public function buscarActivas()
    {
        return $this
            ->createQueryBuilder('d')
            ->select('d')
            ->where('d.publicar = true')
            ->orderBy('d.fecha', 'DESC')
            ->getQuery()
            ->getResult()
        ;        
    }     
}
