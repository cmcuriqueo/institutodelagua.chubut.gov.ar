<?php

namespace APD\Saquito\MultimediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entidad encargada de el manejo de las 'Galerías' 
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="APD\Saquito\MultimediaBundle\Entity\saquitoGaleriasRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoGalerias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="detalle", type="string", length=255, nullable=true)
     */
    private $detalle;

    /**
     * @ORM\OneToMany(targetEntity="APD\Saquito\MultimediaBundle\Entity\saquitoGaleriasfotos", mappedBy="galeria", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    protected $fotos;

    /**
     * @var boolean $publicar
     * 
     * @ORM\Column(name="publicar", type="boolean", nullable=true)
     */
    private $publicar;

    /**
     * @var \DateTime $fecha
     * 
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return saquitoGalerias
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get detalle
     *
     * @return string 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return saquitoGalerias
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function __construct() {
        $this->fotos = new ArrayCollection();
    }    
    
    /**
     * Add foto
     *
     * @param APD\Saquito\MultimediaBundle\Entity\saquitoGaleriasfotos $foto
     * @return saquitoGalerias
     */
    public function addFotos(\APD\Saquito\MultimediaBundle\Entity\saquitoGaleriasfotos $foto)
    {
        $this->fotos[] = $foto;
        return $this;
    }

    /**
     * Remove foto
     *
     * @param APD\Saquito\MultimediaBundleBundle\Entity\saquitoGaleriasfotos $foto
     */
    public function removeFotos(\APD\Saquito\MultimediaBundle\Entity\saquitoGaleriasfotos $foto)
    {
        $this->fotos->removeElement($foto);
    }
    
    /**
     * Get fotos
     *
     */
    public function getFotos()
    {
        return $this->fotos;
    }

    public function setFotos($fotos)
    {
        $this->fotos = $fotos;
    }
    
    /**
     * Set publicar
     *
     * @param boolean $publicar
     * @return saquitoGalerias
     */
    public function setPublicar($publicar)
    {
        $this->publicar = $publicar;

        return $this;
    }
    
    /**
     * Get publicar
     *
     * @return boolean
     */
    public function getPublicar()
    {
        return $this->publicar;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return saquitoGalerias
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }
    
}
