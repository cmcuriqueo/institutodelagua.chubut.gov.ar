<?php

namespace APD\Saquito\MultimediaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Paquete encargado de la administración de contenido multimedia
 *
 * 
 * @category Bundle
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDSaquitoMultimediaBundle extends Bundle
{
}
