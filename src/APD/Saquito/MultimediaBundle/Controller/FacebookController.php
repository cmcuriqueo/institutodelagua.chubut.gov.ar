<?php

namespace APD\Saquito\MultimediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use APD\Saquito\PaginasBundle\Entity\saquitoPaginas;

/**
 * Controlador para Páginas con contenido de Facebook
 *
 * @Route("/facebook")
 * 
 * @category Controladores
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class FacebookController extends Controller
{
    /**
     * Renderiza una página con los albums de Facebook del proyecto
     * 
     * @Route("/albums", name="saquito_facebook_albums")
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @return string Código HTML de la página
     */
    public function albumsAction() {
        
        $albums = $this->container->get('saquitoFacebook')->listaAlbumsPagina();
        
        return $this->render('APDSaquitoMultimediaBundle:Facebook:_albums.html.twig', array(
            'albums' => $albums
        ));
    }    
    
    /**
     * Renderiza una página con los las fotos del Album de Facebook indicado
     * 
     * @Route("/{idAlbum}/fotos", name="saquito_facebook_album_fotos")
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string $idAlbum Id del Album elegido
     * 
     * @return string Código HTML de la página
     */
    public function fotosAction($idAlbum) {
        
        $fotos = $this->container->get('saquitoFacebook')->fotosAlbum($idAlbum);
        $pagina = new saquitoPaginas;
        $pagina->setTitulo('Galería de fotos de Facebook');
        return $this->render('APDSaquitoMultimediaBundle:Facebook:veralbum.html.twig', array(
            'pagina' => $pagina,
            'fotos' => $fotos
        ));
    }    
}
