<?php
// APD\Saquito\MainBundle\RedesSociales\saquitoGoogle.php

namespace APD\Saquito\MainBundle\RedesSociales;

/**
 * Brinda el servicio de interacción con Google
 *
 * 
 * @category Servicios
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoGoogle
{
    
    public function __construct() {
    
        
    }

    /**
     * Función de prueba, todavía no lo tengo implementado.-
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     */
    public function prueba() {

        $client = new Google_Client();
        $client->setApplicationName("Client_Library_Examples");
        $client->setDeveloperKey("AIzaSyC0HdzEaFPjcZfyyNmbUMARGITzXrZq1Yg");

        $service = new Google_Service_Books($client);
        $optParams = array('filter' => 'free-ebooks');
        $results = $service->volumes->listVolumes('Henry David Thoreau', $optParams);

        foreach ($results as $item) {
            echo $item['volumeInfo']['title'], "<br /> \n";
        }        
    }
    
}
