<?php
// APD\Saquito\MainBundle\RedesSociales\saquitoYoutube.php

namespace APD\Saquito\MainBundle\RedesSociales;

/**
 * Brinda el servicio de interacción con Youtube
 *
 * 
 * @category Servicios
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoYoutube
{
    public function __construct() {
    
    }

    /**
     * Devuelve un array con los id de los $cantidad últimos videos del canal $canal
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string $canal Id del canal de Youtube
     * @param string $cantidad Cantidad de videos a devolver
     * 
     * @return array Arreglo con el s de videos encontrados
     */    
    public function listaVideos($canal,$cantidad){
        $videos = array();
        try{
            $youtube = new \SimpleXMLElement('https://www.youtube.com/feeds/videos.xml?channel_id=' . $canal, 0, true);
            for ($i=0;$i<$cantidad;$i++) {
                $entry = $youtube->entry[$i];
                $ns = $entry->getNameSpaces(true);
                $yt = $entry->children($ns['yt']);
                $v = $yt->videoId;
                $videos[] = $v;
            }
        } catch(Exception $e){}

        return $videos;
    }        
}



