<?php
// APD\Saquito\MainBundle\RedesSociales\saquitoTwitter.php

namespace APD\Saquito\MainBundle\RedesSociales;
require_once('twitteroauth/twitteroauth.php');

/**
 * Brinda el servicio de interacción con Twitter
 *
 * 
 * @category Servicios
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoTwitter
{
    private $consumerKey;
    private $consumerSecret;
    private $oAuthToken;
    private $oAuthSecret;
    private $container;
    private $tweet;
    
    public function __construct($container) {
        
        $this->container = $container;
         
        $misParametros = $this->container->getParameter('proyecto')['twitter'];

        $this->consumerKey = $misParametros['consumerKey'];
        $this->consumerSecret = $misParametros['consumerSecret'];
        $this->oAuthToken = $misParametros['oAuthToken'];
        $this->oAuthSecret = $misParametros['oAuthSecret'];

        $this->tweet = new \TwitterOAuth(
            $this->consumerKey,
            $this->consumerSecret,
            $this->oAuthToken,
            $this->oAuthSecret
        );
    }

    /**
     * Publica un mensaje en la cuenta de Twitter del proyecto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string $mensaje Mensaje a publicar
     * @return boolean Publicado o no publicado
     */
    public function publicarMensaje($mensaje){
        return $this->tweet->post('statuses/update', array(
            'status' => $mensaje 
            ));
    }
    
    /**
     * Publica una foto en la cuenta de Twitter del proyecto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string $foto Foto a publicar
     * @return boolean Publicado o no publicado
     */
    public function publicarFoto($foto){

            return $this->tweet->post('statuses/update_with_media', array(
                'status' => $foto
                ));
    }        
}
