<?php
// APD\Saquito\MainBundle\RedesSociales\saquitoFacebook.php

namespace APD\Saquito\MainBundle\RedesSociales;

/**
 * Brinda el servicio de interacción con Facebook
 *
 * 
 * @category Servicios
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoFacebook 
{
    private $appId;
    private $secret;
    private $cookie;
    private $accesstoken;
    private $idPagina;    
    private $facebook;
    private $locale;
    private $container;
    
    public function __construct($container) {
        
        $this->container = $container;
        
        $misParametros = $this->container->getParameter('proyecto')['facebook'];

        $this->appId = $misParametros['appid'];
        $this->secret = $misParametros['secret'];
        $this->cookie = $misParametros['cookie'];
        $this->accesstoken = $misParametros['accesstoken'];
        $this->idPagina = $misParametros['idpagina'];
        $this->locale = $misParametros['locale'];
        
        $this->facebook = new \Facebook\Facebook([
            'app_id' => $this->appId,
            'app_secret' => $this->secret,
            'default_graph_version' => 'v2.3',
            'default_access_token' => $this->accesstoken, // optional
        ]);
    }

    /**
     * Publica un mensaje en la cuenta de Facebook del proyecto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string $mensaje El mensaje que quiero colocar
     * @param string $nombre Description
     * @param string $link Link al que tiene que llevar la publicación
     * @param string $descripcion Descripción del mensaje
     * @param string $imagen URL de la imagen a mostrar en la publicación
     * 
     * @return boolean Publicado o no publicado
     */    
    public function publicarMensaje($mensaje,$nombre,$link,$descripcion,$imagen){

        $parametros =  array(
            'access_token' => $this->accesstoken,
            'message' => $mensaje,
            'name' => $nombre,
            'link' => $link,
            'default_graph_version' => 'v2.2',
            'description' => $descripcion,
            'picture' => $imagen);

        try {
            $this->facebook->post('/'. $this->idPagina .'/feed',$parametros);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph devolvió un error: ' . $e->getMessage();
            return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK devolvió un error: ' . $e->getMessage();
            return false;
        }
        return  true;
        
    }
    
    /**
     * Publicar una foto en la cuenta de Facebook del proyecto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string $foto URL de la foto a publicar
     * @param string $descripcion Descripción de la foto
     * @param string $albumdestino Album de Facebook donde debe subirse la foto
     * 
     * @return boolean Publicado o no publicado
     */
    public function publicarFoto($foto,$descripcion,$albumdestino) {
        
        $parametros =  array(
            'access_token' => $this->accesstoken,
            'message' => $descripcion,
            'source' => $this->facebook->fileToUpload($foto));

        $albums = $this->facebook->get('/'. $this->idPagina .'/albums');
        var_dump($albums);
        
        foreach ($albums['data'] as $album) {
            if($album['description'] == $albumdestino){
                $album_uid = $album['id'];
            }
	}

        try {
            if (isset($album_uid)) { // Encontró el album
                $this->facebook->post('/'. $album_uid .'/photos',$parametros);
            } else { // no encontró el album
                $this->facebook->post('/'. $this->idPagina .'/photos',$parametros);
            }
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph devolvió un error: ' . $e->getMessage();
            return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK devolvió un error: ' . $e->getMessage();
            return false;
        }
        return  true;
    }
    
    /**
     * Borrar función?? 
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param type $name Description
     * @return type Description
     */
    public function publicarFotos($foto,$descripcion,$albumdestino){
//
//        $this->facebook->setFileUploadSupport(true);
//
//        
//        $albums = $this->facebook->api('/'. $this->idPagina .'/albums');
//	foreach ($albums['data'] as $album) {
//            if($album['name'] == $albumdestino){
//                $album_uid = $album['id'];
//            }
//	}
//        $parametros = array (
//            'access_token' => $this->accesstoken,
//            'message' => $descripcion,
//            'image' => '@' . $foto,
//        );
//        if (isset($album_uid)) { // Encontró el album
//            $this->facebook->api('/'. $album_uid .'/photos', 'post', $parametros);
//        } else { // no encontró el album
//            $this->facebook->api('/'. $this->idPagina .'/photos', 'post', $parametros);
//        }
    }    
    
    /**
     * Obtener la lista de Albums de la cuenta de Facebook del proyecto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return array Matriz con la lista de Albums [i][id,nombre,cantidad,portada,fecha]
     */
    public function listaAlbumsPagina(){
        $i = 0;
        $misAlbums = null;

        $resultado = @file_get_contents('https://graph.facebook.com/' . $this->idPagina .  '/albums?access_token=' . $this->accesstoken);
 
        if ($resultado == null) {
            return null;
        }
        
        $albums = json_decode($resultado,true);
        foreach($albums['data'] as $album)
        {   
            if ($album['type'] == 'normal') {
                $misAlbums[$i]["id"] = $album['id'];
                $misAlbums[$i]["nombre"] = $album['name'];

                if (isset($album["count"])){
                    $misAlbums[$i]["cantidad"] = $album["count"];
                } else {
                    $misAlbums[$i]["cantidad"] = 0;
                }            
                $portada = json_decode(@file_get_contents('https://graph.facebook.com/' .$album['id']. '?fields=picture'),true);
                $misAlbums[$i]["portada"] = $portada['picture']['data']['url'];
                $misAlbums[$i]["fecha"] = $album['updated_time'];
                $i = $i +1;
            }
        }
        return  $misAlbums; 
    }
    
    /**
     * Obtener las fotos de un determinado Album de Facebook
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string $idAlbum Id del Album a obtener las fotos
     * @return array Matriz con los datos de las fotos [i][id,source,picture,link,detalles]
     */
    public function fotosAlbum($idAlbum) {
        $i = 0;
        $misFotos = null;
                
        $fotos = json_decode(@file_get_contents('https://graph.facebook.com/' . $idAlbum . '/photos'),true);

        if ($fotos) {
            foreach($fotos['data'] as $foto)
            {
                $misFotos[$i]["id"] = $foto["id"];
                $misFotos[$i]["source"] = $foto["source"];
                $misFotos[$i]["picture"] = $foto["picture"];
                $misFotos[$i]["link"] = $foto["link"];
                if (isset($foto["name"])){
                    $misFotos[$i]["detalles"] = $foto["name"];
                } else {
                    $misFotos[$i]["detalles"] = " ";
                }
                $i = $i +1;
            }        
        }

        return $misFotos;                
    }

}
