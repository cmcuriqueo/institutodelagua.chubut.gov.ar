<?php

namespace APD\Saquito\MainBundle\Utiles;

/**
 * Brinda el servicio de utilidades para el clima
 *
 * 
 * @category Utilidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class utilesClima
{
    private $localidad;
    private $temperatura;
    private $minima;
    private $maxima;
    private $imagenchica;    
    private $imagengrande;
    private $locale;
    private $codigo;
    private $fecha;
    private $amanecer;
    private $ocaso;
    private $viento;
    private $dianoche; // d= día / n= noche
    
    public function __construct() {
//        No puedo leer los parametros, por ahora dejo los valores dentro de la clase,...
        $this->localidad = "467039"; // Rawson
        $this->temperatura = "";
        $this->minima = "";
        $this->maxima = "";
        $this->amanecer = "";
        $this->ocaso = "";
        $this->viento = "";
        $this->imagenchica = "";
        $this->imagengrande = "";
        $this->locale = "es_ES";
        
    }

    public function leerClima($localidad) {
        $BASE_URL = "http://query.yahooapis.com/v1/public/yql";
        $yql_query = 'select * from weather.forecast where woeid =' . $localidad . ' and u="c"';
        $yql_query_url = $BASE_URL . "?q=" . urlencode($yql_query) . "&format=json";
        // Make call with cURL
        $session = curl_init($yql_query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER,true);
        $json = curl_exec($session);
        // Convert JSON to PHP object
        $phpObj =  json_decode($json);
//        var_dump($phpObj);
        $datosClima = $phpObj->{'query'}->{'results'}->{'channel'}->{'item'}->{'condition'}; 
        $datosViento = $phpObj->{'query'}->{'results'}->{'channel'}->{'wind'}; 
        $datosAstronimia = $phpObj->{'query'}->{'results'}->{'channel'}->{'astronomy'}; 
        $datosPronostico = $phpObj->{'query'}->{'results'}->{'channel'}->{'item'}->{'forecast'}; 
        
        if (isset($datosClima)) {
            $this->codigo = $datosClima->{'code'};
            $this->temperatura =  $datosClima->{'temp'};
//            $this->fecha  = date('d/m/Y H:i:s',strtotime( $datosClima->{'date'}));	        
            $this->fecha  = $datosClima->{'date'};	        
            $this->localidad = $localidad; // Rawson        
            $this->amanecer = $datosAstronimia->{'sunrise'};
            $this->ocaso = $datosAstronimia->{'sunset'};
            $this->viento = $datosViento->{'speed'} . "Km/h";
            $ahora =  time();
            $salida = strtotime(date('Y-m-d') . " " . $datosAstronimia->{'sunrise'});
            $puesta = strtotime(date('Y-m-d') . " " . $datosAstronimia->{'sunset'});
            if($ahora > $salida && $ahora < $puesta) {
                $this->dianoche = 'd';
            } else {
                $this->dianoche = 'n';
            }
//            echo($this->dianoche);
            $this->minima = $datosPronostico[0]->{'low'};
            $this->maxima = $datosPronostico[0]->{'high'};
        } else {
            $this->codigo = "-";
            $this->temperatura = "-";
            $this->fecha  = date('d/m/Y h:i:s');	        
            $this->localidad = $localidad; // Rawson
            $ahora =  time();
            $this->dianoche = 'd';
        }
        $this->locale = "es_ES";
    }

    public function getFecha(){
        return $this->fecha;
    }
    
    public function getLocalidad(){
        return $this->localidad;
    }
    public function getTemperatura(){
        return $this->temperatura . "ºC";
    }
    public function getMinima(){
        return $this->minima . "ºC";
    }
    public function getMaxima(){
        return $this->maxima . "ºC";
    }
    public function getAmanecer(){
        return $this->amanecer;
    }
    public function getOcaso(){
        return $this->ocaso;
    }
    public function getViento(){
        return $this->viento;
    }

    public function getImagenchica(){
        if ($this->codigo != "-") {
            return "http://l.yimg.com/a/i/us/nws/weather/gr/" . $this->codigo . $this->dianoche . "s.png";
        }else{
            return "";
        }
    }
    public function getImagengrande(){
        return "http://l.yimg.com/os/mit/media/m/weather/images/icons/l/" . $this->codigo . $this->dianoche . "-100567.png";
    }
    public function getFondo(){
        return "http://l.yimg.com/os/mit/media/m/weather/images/icons/bkgnd/" . $this->codigo . $this->dianoche . "-791140.jpg";
    }
    
    public function getDianoche(){
        return $this->dianoche;
    }
    
    public function getEstado() {
        $mensajes = array(
            0 => 'Tornado',
            1 => 'Tormenta Tropical',
            2 => 'Hurac&aacute;n',
            3 => 'Tormentas El&eacute;ctricas Severas',
            4 => 'Tormentas El&eacute;ctricas',
            5 => 'Lluvia y Nieve',
            6 => 'Lluvia y Aguanieve',
            7 => 'Nieve y Aguanieve',
            8 => 'Llovizna congelada',
            9 => 'Llovizna',
            10 => 'Lluvia congelada',
            11 => 'Lluvia',
            12 => 'Lluvia',
            13 => 'R&aacute;fagas de Nieve',
            14 => 'Nevada ligera',
            15 => 'Nieve con viento',
            16 => 'Nieve',
            17 => 'Granizo',
            18 => 'Aguanieve',
            19 => 'Polvo',
            20 => 'Neblina',
            21 => 'Niebla ligera',
            22 => 'Neblina', //no creo que sea exacta esta traduccion
            23 => 'Ventoso',
            24 => 'Con viento',
            25 => 'Helado',
            26 => 'Nublado',
            27 => 'Muy nublado',
            28 => 'Muy nublado',
            29 => 'Parcialmente nublado',
            30 => 'Parcialmente nublado',
            31 => 'Despejado',
            32 => 'Soleado',
            33 => 'Despejado',
            34 => 'Despejado',
            35 => 'Lluvia y Granizo',
            36 => 'Caluroso',
            37 => 'Tormentas el&eacute;ctricas aisladas',
            38 => 'Tormentas el&eacute;ctricas dispersas',
            39 => 'Tormentas el&eacute;ctricas dispersas',
            40 => 'Lluvia dispersa',
            41 => 'Nieve densa',
            42 => 'Nieve y lluvia dispersas',
            43 => 'Nieve densa',
            44 => 'Parcialmente nublado',
            45 => 'Tormentas el&eacute;ctricas',
            46 => 'Nieve',
            47 => 'Tormentas el&eacute;ctricas aisladas',
            3200 => 'No disponible',
	);
        if ($this->codigo != "-") {
            return $mensajes[$this->codigo];
        }else{
            return "S/D";
        }        
        
    }
    
}
