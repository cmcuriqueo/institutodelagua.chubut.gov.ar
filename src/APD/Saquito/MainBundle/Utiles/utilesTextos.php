<?php

namespace APD\Saquito\MainBundle\Utiles;

/**
 * Brinda el servicio de utilidades para textos
 *
 * 
 * @category Servicios
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class utilesTextos
{
    /**
     * Devuelve el SLUG de una cadena de texto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string $cadena Cadena a procesar
     * 
     * @return string Cadena en formato SLUG
     */
    public static function slugify($cadena) {
        $characters = array(
            "Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u", 
            "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
            "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u"
        );

        $cadena = strtr($cadena, $characters); 
        $cadena = strtolower(trim($cadena));
        $cadena = preg_replace("/[^a-z0-9-]/", "-", $cadena);
        $cadena = preg_replace("/-+/", "-", $cadena);
        if(substr($cadena, strlen($cadena) - 1, strlen($cadena)) === "-") {
            $cadena = substr($cadena, 0, strlen($cadena) - 1);
        }
        return $cadena;
    }
    
}