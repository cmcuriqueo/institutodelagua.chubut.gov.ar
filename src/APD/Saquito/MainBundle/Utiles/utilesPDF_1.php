<?php

namespace APD\BaseBundle\Utiles;

require_once('tcpdf\\tcpdf.php');
require_once('fpdi\\fpdi.php');

class utilesPDF extends \FPDI
{
    private $plantilla;
    private $nombre;
    private $titulo;
    
    public function setNombre ($nombre)
    {
        $this->nombre = $nombre;
    }
    
    public function setTitulo ($titulo)
    {
        $this->titulo = $titulo;
    }

    public function setPlantilla ($plantilla)
    {
        $this->plantilla = $plantilla;
    }
    
    public function aplicarPlantilla($orientacion)
    {
        $this->setSourceFile('plantillas/' . $orientacion . '-' . $this->plantilla);
        $tplIdx = $this->importPage(1); 
        $this->useTemplate($tplIdx); 
    }

    function Header()
    {
//        // Logo
//        if ($this->CurOrientation == 'L')
//        {
//            $this->Image('bundles/web/images/isologo.png',240,10,33);
//            $this->line(15,23,280,23);
//        } else {
//            $this->Image('bundles/web/images/isologo.png',160,10,33);
//            $this->line(15,23,205,23);
//        }
//
        switch ($this->plantilla) {
            case 1:
                if ($this->CurOrientation == 'L')
                {
                    $this->Image('bundles/web/images/isologo.png',240,10,33);
                    $this->line(15,23,280,23);
                } else {
                    $this->Image('bundles/web/images/isologo.png',160,10,33);
                    $this->line(15,23,205,23);
                }
                break;
            case 2:
                if ($this->CurOrientation == 'L')
                {
                    $this->Image('bundles/web/images/isologo.png',240,10,33);
                    $this->line(15,23,280,23);
                    $this->Image('bundles/web/images/isologo.png',110,10,33);
                } else {
                    $this->Image('bundles/web/images/isologo.png',160,10,33);
                    $this->line(15,23,205,23);
                    $this->Image('bundles/web/images/isologo.png',160,145,33);
                    $this->line(15,160,205,160);
                }
                break;
            case 3:
                if ($this->CurOrientation == 'L')
                {
                    $this->Image('bundles/web/images/isologo.png',65,10,33);
                    $this->Image('bundles/web/images/isologo.png',165,10,33);
                    $this->Image('bundles/web/images/isologo.png',250,10,33);
                    $this->line(10,23,285,23);
                } else {
                    $this->Image('bundles/web/images/isologo.png',160,10,33);
                    $this->Image('bundles/web/images/isologo.png',160,98,33);
                    $this->Image('bundles/web/images/isologo.png',160,198,33);
                    $this->line(15,20,205,20);
                    $this->line(15,110,205,110);
                    $this->line(15,210,205,210);
                }
                break;
        }        
        
    }
    
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('dejavusans','',8);
        $this->Cell(0,10,utf8_decode($this->nombre),0,0,'L');
        
        // Número de página
       $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
//        $this->setFooterData(array(0,64,0), array(0,64,128));
        
    }
    
    public function importarArchivos($archivos)
    {
        foreach($archivos AS $archivo) {
            $cantidadDePaginas = $this->setSourceFile($archivo);
            for ($i = 1; $i <= $cantidadDePaginas; $i++) {
                $tplIdx = $this->ImportPage($i);
                $s = $this->getTemplatesize($tplIdx);
                if ($s['w'] > $s['h']) {
                    // Es Apaisado
                    $orientacion = 'L';
                } else {
                    $orientacion = 'P';
                }
                $this->AddPage($orientacion, array($s['w'], $s['h']));
                $this->useTemplate($tplIdx);
            }
        }
    }
}
