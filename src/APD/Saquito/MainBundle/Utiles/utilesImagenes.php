<?php

namespace APD\Saquito\MainBundle\Utiles;

/**
 * Brinda el servicio de utilidades para imagenes
 *
 * 
 * @category Servicios
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class utilesImagenes 
{
    /**
     * Le agrega una marca de agua a una foto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string $foto URL de la foto
     * @param string $firma URL de la marca de agua
     * 
     * @return boolean True si salio todo bien
     */
    public static function firmarFoto($foto,$firma){
        // Cargar la estampa y la foto para aplicarle la marca de agua
        $estampa = imagecreatefrompng($firma);
        $im = imagecreatefromjpeg($foto);

        // Establecer los márgenes para la estampa y obtener el alto/ancho de la imagen de la estampa
        $margen_dcho = 0;
        $margen_inf = 10;
        $sx = imagesx($estampa);
        $sy = imagesy($estampa);

        // Copiar la imagen de la estampa sobre nuestra foto usando los índices de márgen y el
        // ancho de la foto para calcular la posición de la estampa. 
        imagecopy($im, $estampa, imagesx($im) - $sx - $margen_dcho, imagesy($im) - $sy - $margen_inf, 0, 0, imagesx($estampa), imagesy($estampa));

        // Imprimir y liberar memoria
        //header('Content-type: image/png');
        imagejpeg($im,$foto, 100);
        return true;
    }
    
    /**
     * Si el tamaño de la foto (en ancho o largo) es mayor que máximo, la redimensiona
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param strinf $nombre URL de la foto
     * @param integer $maximo Pixels máximos que puede tener la foto
     * 
     * @return boolean True si salio todo bien
     */
    public static function redimensionarFoto($nombre,$maximo){
        $miniatura_ancho_maximo = $maximo;
        $miniatura_alto_maximo = $maximo;
        $ruta_imagen = $nombre;
        $info_imagen = getimagesize($ruta_imagen);
        $imagen_ancho = $info_imagen[0];
        $imagen_alto = $info_imagen[1];
        $imagen_tipo = $info_imagen['mime'];
        $proporcion_imagen = $imagen_ancho / $imagen_alto;
        $proporcion_miniatura = $miniatura_ancho_maximo / $miniatura_alto_maximo;
        if ( $proporcion_imagen > $proporcion_miniatura ){
                $miniatura_ancho = $miniatura_ancho_maximo;
                $miniatura_alto = $miniatura_ancho_maximo / $proporcion_imagen;
        } else if ( $proporcion_imagen < $proporcion_miniatura ){
                $miniatura_ancho = $miniatura_ancho_maximo * $proporcion_imagen;
                $miniatura_alto = $miniatura_alto_maximo;
        } else {
                $miniatura_ancho = $miniatura_ancho_maximo;
                $miniatura_alto = $miniatura_alto_maximo;
        }
        switch ( $imagen_tipo ){
                case "image/jpg":
                case "image/jpeg":
                        $imagen = imagecreatefromjpeg( $ruta_imagen );
                        break;
                case "image/png":
                        $imagen = imagecreatefrompng( $ruta_imagen );
                        break;
                case "image/gif":
                        $imagen = imagecreatefromgif( $ruta_imagen );
                        break;
        }
        $lienzo = imagecreatetruecolor( $miniatura_ancho, $miniatura_alto );
        imagecopyresampled($lienzo, $imagen, 0, 0, 0, 0, $miniatura_ancho, $miniatura_alto, $imagen_ancho, $imagen_alto);
        imagejpeg($lienzo, $ruta_imagen, 100);
        
        return true;
    }
}
