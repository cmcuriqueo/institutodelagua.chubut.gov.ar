<?php

namespace APD\Saquito\MainBundle\Utiles;

require_once(__DIR__.'/recaptcha/recaptchalib.php');

/**
 * Brinda el servicio de Recaptcha de Google para los formularios.
 *
 * 
 * @category Utilidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class utilesRecaptcha extends \ReCaptcha
{

    
}
