<?php

// src/APD/Saquito/MainBundle/DataFixtures/ORM/LoadInicial.php

namespace APD\Saquito\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use APD\Saquito\MainBundle\Entity\saquitoLocalidades;
use APD\Saquito\MainBundle\Entity\saquitoProvincias;
use APD\Saquito\MainBundle\Entity\saquitoPaises;

/**
 * Carga los datos de prueba de las tablas varias
 *
 * 
 * @category Datafixtures
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class LoadInicial extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder()
    {
        return 2;
    }

    public function load(ObjectManager $manager)
    {

        $yaml = new Parser();
        
    // Leer el proyecto actual
        $proyecto = $this->container->getParameter('proyecto')['nombre'];


    // Inicializar Paises    
        $collecionpaises = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/MainBundle/paises.yml'));
        foreach ($collecionpaises['paises'] as $datos) {
            $pais = new saquitoPaises;
            $pais->setDescripcion($datos['descripcion']);

            $manager->persist($pais);
            
            $this->addReference('pais-'.$datos['descripcion'], $pais);
        }

    // Inicializar provincias    
        $collecionprovincias = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/MainBundle/provincias.yml'));
        foreach ($collecionprovincias['provincias'] as $datos) {
            $provincia = new saquitoProvincias;
            $provincia->setDescripcion($datos['descripcion']);
            $provincia->setPais($this->getReference('pais-' . $datos['pais']));

            $manager->persist($provincia);
            
            $this->addReference('provincia-'.$datos['descripcion'], $provincia);
        }
        
    // Inicializar localidades    
        $collecionlocalidades = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/MainBundle/localidades.yml'));
        foreach ($collecionlocalidades['localidades'] as $datos) {
            $localidad = new saquitoLocalidades;
            $localidad->setDescripcion($datos['descripcion']);
            $localidad->setProvincia($this->getReference('provincia-' . $datos['provincia']));

            $manager->persist($localidad);
            
        }
        
        
        $manager->flush();
    }

}