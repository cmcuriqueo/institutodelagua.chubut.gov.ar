<?php

namespace APD\Saquito\MainBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use APD\Saquito\PaginasBundle\Entity\saquitoPaginas;

/**
 * Controlador principal del Bundle Main
 *
 * @Route("/")
 * 
 * @category Controladores
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class MainController extends Controller
{

    /**
     * Renderiza la página de portada del Proyecto
     * 
     * @Route("/", name="saquito_paginas_bienvenidos") 
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return string Código HTML de la portada del proyecto
     */
    public function bienvenidosAction()
    {
//        return $this->redirectToRoute('cultura_presentaciones_portada');
        
        $mobileDetector = $this->get('mobile_detect.mobile_detector');
        $request = $this->getRequest();
        
        $request->setLocale('es');
        $em = $this->getDoctrine()->getManager();
        $noticias = $em->getRepository('APDSaquitoNoticiasBundle:saquitoNoticias')
                ->recuperarUltimas($request->getLocale(),$this->get('security.authorization_checker')->isGranted('ROLE_USER'));

        $pagina = new saquitoPaginas();
        
        $pagina->setTitulo("Portada");
        $pagina->setDescripcion($this->container->getParameter('proyecto')['descripcion']);
        $pagina->setRobots("Index, Follow");
        $pagina->setKeyworks($this->container->getParameter('proyecto')['keyworks']);
        $pagina->setAutor($this->container->getParameter('proyecto')['autor']);
        
        if ( $mobileDetector->isMobile()) {
            $template = "APDSaquitoPaginasBundle:Plantillas:portada_movil.html.twig";
        } else {
            $template = "APDSaquitoPaginasBundle:Plantillas:portada_desktop.html.twig";
        }
        
        return $this->render(
                $template, 
                array('pagina' => $pagina, 'noticias' =>$noticias));
    }

}
