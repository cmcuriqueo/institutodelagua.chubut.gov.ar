<?php

namespace APD\Saquito\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use APD\Saquito\MainBundle\Form\saquitoFormBuscador;

/**
 * Controlador para las busquedas dentro del proyecto
 *
 * @Route("/buscador")
 * 
 * @category Controlador
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class BuscadorController extends Controller
{
   

    /**
     * Renderiza el formularios de busqueda
     * 
     * @Route("/", name="saquito_buscador_formulario") 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @return string Código HTML del formulario de busqueda
     */
    public function buscarAction(){
        $pagination = null;
        $categoria = "Todo";
        return $this->render('APDSaquitoMainBundle:Buscador:resultado.html.twig', 
                array(
                    'criterio' => null,
                    'pagination' => $pagination
                ));
    }
    
    /**
     * Realiza la busqueda dentro de la base de datos
     * 
     * @Route("/resultado/{criterio}", name="saquito_buscador_buscar") 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @return string Código HTML del listado de resultado de la busqueda
     */
    public function resultadoAction($criterio,Request $request){
        $resultados = [];
        $lista_categorias = $this->container->getParameter('proyecto')['buscador'];
        $em = $this->getDoctrine()->getManager();
        $permisos = $this->get('security.authorization_checker')->isGranted('ROLE_USER');
        foreach ($lista_categorias as $cat) {
            $resultados = \array_merge($resultados , $em->getRepository($cat['entidad'])->buscar($permisos,$criterio));
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $resultados,
            $request->query->get('pagina', 1),
            20
        );
        
        return $this->render('APDSaquitoMainBundle:Buscador:resultado.html.twig', 
                array(
                    'lista_categorias' => $lista_categorias,
                    'criterio' => $criterio,
                    'pagination' => $pagination
                ));

    }
   
}
