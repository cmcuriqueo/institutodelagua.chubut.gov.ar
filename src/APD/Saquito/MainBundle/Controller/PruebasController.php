<?php

namespace APD\Saquito\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controlador de uso sólo para pruebas en el desarrollo
 *
 * 
 * @Route("/pruebas")

 * @category Controlador
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class PruebasController extends Controller
{
    
    /**
     * @Route("/google", name="saquito_prueba_google")
     */
    public function googleAction(){

        $client = new \Google_Client();
        $client->setApplicationName("Chubut Deportes");
        $client->setDeveloperKey("AIzaSyC0HdzEaFPjcZfyyNmbUMARGITzXrZq1Yg");

        $service = new \Google_Service_Books($client);
        $optParams = array('filter' => 'free-ebooks');
        $results = $service->volumes->listVolumes('Henry David Thoreau', $optParams);

        foreach ($results as $item) {
            echo $item['volumeInfo']['title'], "<br /> \n";
        } 
    }
    
    
    /**
     * @Route("/paginasegura", name="saquito_prueba_pagina_segura")
     * @Security("has_role('ROLE_ADMIN')")
     * 
     */     
    public function paginaSegura()
    {
        return new Response('Hola <b>' . $this->getUser()->getUsername() . "</> !");
    }
    
    /**
     * @Route("/facebook", name="saquito_prueba_facebook")
     */    
    public function facebookAction() {
        
//        $resultado= $this->container->get('saquitoFacebook')->publicarMensaje(
//                "SAN LORENZO 3 - SARMIENTO 0",
//                "“Siempre trato de buscar el gol”",
//                "http://www.ole.com.ar/san-lorenzo/",
//                "Como ante Newell’s y Viale, Martín Cauteruccio se anotó en el score: hizo los últimos dos goles. Está on fire. ",
//                "http://www.ole.com.ar/san-lorenzo/Cauteruccio-encendido-Sarmiento-clavo-doblete_OLEIMA20150524_0073_3.jpg"
//            );
        
        $resultado = $this->container->get('saquitoFacebook')->publicarFoto(
                "http://uploads.diariojornada.com.ar/Imagenes/2015/5/25/hd/464E63636363466658385875376A3745543874566D413D3D_slider.jpg",
                "Paro!",
                "album"
                );
        
        return $this->render('APDSaquitoMainBundle:Pruebas:facebook.html.twig', array(
            'resultado' => $resultado
        ));
    }

    /**
     * @Route("/youtube", name="saquito_prueba_youtube")
     */    
    public function youtubeAction() {

        $channelId = 'UCHLpTxsPLAKMgODmx1-51kA';
        $videos = null;
        $videos = $this->container->get('saquitoYoutube')->listaVideos($channelId,4);
        return $this->render('APDSaquitoMainBundle:Pruebas:youtube.html.twig', array(
            'videos' => $videos
        ));
    }

    
}
