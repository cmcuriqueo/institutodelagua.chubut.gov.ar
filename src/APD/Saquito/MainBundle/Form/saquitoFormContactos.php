<?php

namespace APD\Saquito\MainBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

/**
 * Arma el formulario de contacto
 *
 *
 * @category Formularios
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoFormContactos extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nombre')
            ->add('email', 'email')
            ->add('telefono')
            ->add('asunto')
            ->add('cuerpo', 'textarea')
            ->add('enviar', 'submit');;
    }

    public function getName()
    {
        return 'apd_saquito_main_bundle_form_contacto';
    }
}
