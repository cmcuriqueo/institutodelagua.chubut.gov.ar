<?php

namespace APD\Saquito\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Clase del formulario de busqueda
 *
 * 
 * @category Formularios
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoFormBuscador extends AbstractType
{
    
    /**
     * Crea el formulario de busqueda
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param FormBuilderInterface $builder Description
     * @param array $options Description
     * @return type Description
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $builder
            ->add('texto', 'text', array('required'  => true))
        ;        

    }

    public function getName()
    {
        return 'apd_saquito_main_bundle_form_busqueda';
    }
}