<?php

namespace APD\Saquito\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entidad encargada de el manejo de las 'Localidades'
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks 
 * @ORM\Entity
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */

class saquitoLocalidades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=60)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="APD\Saquito\MainBundle\Entity\saquitoProvincias", inversedBy="localidades")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName = "id")
     */
    protected $provincia;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return baseLocalidad
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set provincia
     *
     * @param APD\Saquito\MainBundle\Entity\saquitoProvincias $provincia
     * @return baseLocalidad
     */
    public function setProvincia(\APD\Saquito\MainBundle\Entity\saquitoProvincias $provincia = null)
    {
        $this->provincia = $provincia;
        return $this;
    }

    /**
     * Get provincia
     *
     * @return APD\Saquito\MainBundle\Entity\saquitoProvincias
     */
    public function getProvincia()
    {
        return $this->provincia;
    }    
    
    public function __toString() {
        return $this->descripcion;
    }
}
