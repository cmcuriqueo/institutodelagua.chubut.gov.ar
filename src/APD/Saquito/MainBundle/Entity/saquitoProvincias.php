<?php

namespace APD\Saquito\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entidad encargada de el manejo de las 'Provincias'
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks 
 * @ORM\Entity
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */

class saquitoProvincias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=60)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="APD\Saquito\MainBundle\Entity\saquitoPaises", inversedBy="provincias")
     * @ORM\JoinColumn(name="pais_id", referencedColumnName = "id")
     */
    protected $pais;
    
    /**
     * @ORM\OneToMany(targetEntity="APD\Saquito\MainBundle\Entity\saquitoLocalidades", mappedBy="provincia")
     */
    protected $localidades;

    public function __construct() {
        $this->localidades = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return saquitoProvincias
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    /**
     * Set pais
     *
     * @param APD\Saquito\MainBundle\Entity\saquitoPaises $pais
     * @return saquitoProvincias
     */
    public function setPais(\APD\Saquito\MainBundle\Entity\saquitoPaises $pais = null)
    {
        $this->pais = $pais;
        return $this;
    }

    /**
     * Get pais
     *
     * @return APD\Saquito\MainBundle\Entity\saquitoPaises
     */
    public function getPais()
    {
        return $this->pais;
    }    
    public function __toString() {
        return $this->descripcion;
    }    
}
