<?php

namespace APD\Saquito\MainBundle\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

/**
 * Entidad encargada del manejo de los formularios de contacto
 *
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoContactos
{
    protected $nombre;

    protected $email;

    protected $telefono;
    
    protected $asunto;

    protected $cuerpo;

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('nombre', new NotBlank());

        $metadata->addPropertyConstraint('email', new Email());

    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }
    
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getAsunto()
    {
        return $this->asunto;
    }

    public function setAsunto($asunto)
    {
        $this->asunto = $asunto;
    }

    public function getCuerpo()
    {
        return $this->cuerpo;
    }

    public function setCuerpo($cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }
}