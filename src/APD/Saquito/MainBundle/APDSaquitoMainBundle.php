<?php

namespace APD\Saquito\MainBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Paquete principal del proyecto saquito, es la base del proyecto
 *
 * 
 * @category Bundle
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDSaquitoMainBundle extends Bundle
{
}
