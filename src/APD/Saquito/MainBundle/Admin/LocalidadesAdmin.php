<?php
namespace APD\Saquito\MainBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clase para administrar las Localidades (SonataAdmin)
 *
 * 
 * @category Admin
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class LocalidadesAdmin extends Admin
{
    protected $baseRouteName = 'mainlocalidades_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'fecha' // field name
        );    
   
    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    private $container;

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('provincia', 'entity', 
                array(
                    'class' => 'APDSaquitoMainBundle:saquitoProvincias',
                    'property' => 'descripcion',
                ))
                
            ->add('descripcion', null,
                array(
                ))
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('descripcion')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('descripcion')
            ->add('provincia')
            ->add('_action', 'Acciones', array(
                    'actions' => array(
                    'edit' => array(),
                    'delete' => array('label' =>''),
            )));
    }

}