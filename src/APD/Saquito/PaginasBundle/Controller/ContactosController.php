<?php

namespace APD\Saquito\PaginasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use APD\Saquito\MainBundle\Entity\saquitoContactos;
use APD\Saquito\MainBundle\Form\saquitoFormContactos;
use APD\Saquito\MainBundle\Utiles\utilesRecaptcha;

/**
 * Se encarga de los formularios de contacto
 *
 * 
 * @category Controller
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class ContactosController extends Controller
{
    
    /**
     * Renderiza un formulario de contacto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @return string Código HTML del formulario de contacto
     */
    public function armarFormularioAction()
    {
        $contacto = new saquitoContactos();
        $form = $this->createForm(new saquitoFormContactos(), $contacto);
        return $this->render('APDSaquitoPaginasBundle:Contacto:contacto.html.twig', array(
            'form' => $form->createView()
        ));
    }    
    
    /**
     * Procesa el formulario de contacto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @Route("/graciasporcontactarte", name="saquito_paginas_contacto_procesar")
     * 
     * @category function
     * @return string Código HTML de respuesta
     */
    public function procesarContactoAction()
    {
        $resupuestaCaptcha = null;

        $request = $this->getRequest();
        $config = $this->container->getParameter('proyecto');
        $secret = $config['google']['recaptcha'];
        $miCaptcha = new utilesRecaptcha($secret);

        if ($request->getMethod() == 'POST') {
            $contacto = new saquitoContactos();
            $form = $this->createForm(new saquitoFormContactos(), $contacto);
            $form->handleRequest($request);

            if ($request->get("g-recaptcha-response")) {
                $resupuestaCaptcha = $miCaptcha->verifyResponse(
                    $request->server->get('REMOTE_ADDR'),
                    $request->get("g-recaptcha-response"));
            }

            if ($resupuestaCaptcha != null && $resupuestaCaptcha->success) {
                if ($form->isValid()) {
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Mensaje desde el sitio web de ' . $config['titulo'])
                        ->setFrom($config['email']['from'])
                        ->setTo($config['email']['to'])
                        ->setBody($this->renderView('APDSaquitoPaginasBundle:Contacto:contacto.txt.twig', 
                                array(
                                    'contacto' => $contacto
                                ))                          
                                , 'text/html');
                    $this->get('mailer')->send($message);
                    $this->get('session')->getFlashBag()->add('notice', 'Recibimos tu mensaje correctamente!, muchas gracias por comunicarte');
                } else {
                    $this->get('session')->getFlashBag()->add('notice', 'No pudimos recibir el mensaje porque alguno de los datos no son correctos, observa el formulario y vuélvelo a enviar, muchas gracias');
                }
                return $this->render('APDSaquitoPaginasBundle:Contacto:gracias.html.twig');
            } else {
                return $this->render('APDSaquitoPaginasBundle:Contacto:recaptcha.html.twig');        
            }
        } else {
            return $this->redirectToRoute('saquito_paginas_bienvenidos');
        }
    }    
    
}
