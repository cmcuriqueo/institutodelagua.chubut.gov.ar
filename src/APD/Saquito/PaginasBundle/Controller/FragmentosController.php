<?php

namespace APD\Saquito\PaginasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controlador por defecto de los fragmentos
 *
 * 
 * @category Controladores
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */

class FragmentosController extends Controller
{

    /**
     * Función para obtener el código html del fragmento indicado
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param string	SLUG del fragmento del que quiero obtener el código HTML.
     * @return string   Código HTML
     */
    public function mostrarAction($slug)
    {
        $fragmento = $this->getDoctrine()
            ->getRepository('APDSaquitoPaginasBundle:saquitoFragmentos')
            ->findOneByNombre($slug);

        if (!$fragmento) {
            return new Response('<div style="padding:10px;">{Necesitás crear un <i><strong>fragmento</strong></i> con el nombre <b>"'. $slug . '"</b> para que aparezca aquí.}</div>');
        } else {
            return new Response($fragmento->getContenido());
        }
    }
    
}
