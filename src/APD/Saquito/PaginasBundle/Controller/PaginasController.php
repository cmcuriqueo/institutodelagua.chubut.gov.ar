<?php

namespace APD\Saquito\PaginasBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use APD\Saquito\PaginasBundle\Entity\saquitoPaginas;

/**
 * Controlador principal del Bundle Paginas
 *
 * @Route("/")
 *
 * @category Controladores
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class PaginasController extends Controller
{
    /**
     * Renderiza la página indicada
     *
     * @Route("/{_locale}/{id}/{slug}", name="saquito_paginas_ver")
     *
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     *
     * @category function
     * @param saquitoPaginas $pagina Pagina a mostrar
     * @return string Código HTML de la página
     */
    public function verAction(saquitoPaginas $pagina)
    {
        if ( $pagina ) {
            if ($pagina->getActiva()){
                if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') or ($pagina->getPrivado() == false)) {
                    return $this->render(
                            $pagina->getPlantilla()->getDescripcion(),
                            array('pagina' => $pagina));
                } else {
                    throw new \Exception('Esta información tiene acceso restringido.');
                }
            } else {
                throw new \Exception('La página solicitada no está activa!');
            }
        } else {
            throw new \Exception('No existe la página que solicitaste!');
        }
    }

    /**
     * Renderiza una página con el listado de páginas
     *
     * @Route("/listarpaginas", name="saquito_paginas_listar")
     *
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     *
     * @category function
     * @param Request $request  Utiliza knp_paginator, por eso necesita el rango de noticias a mostrar
     * @return string Código HTML de la página con el listado de páginas
     */
    public function listarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $paginas = $em->getRepository('APDSaquitoPaginasBundle:saquitoPaginas')->buscarTodos();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $paginas,
            $request->query->get('pagina', 1),
            $this->container->getParameter('proyecto')['noticias']['porpagina']
        );
        return $this->render('APDSaquitoPaginasBundle:Paginas:listar.html.twig', array('pagination' => $pagination));
    }

}
