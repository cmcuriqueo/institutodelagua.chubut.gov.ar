<?php

namespace APD\Saquito\PaginasBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

//use APD\Saquito\PaginasBundle\Entity\saquitoInstitutional;

/**
 * Controlador para los Links de intituciones las portadas
 *
 * @Route("/", name="saquito_institutional_mostrar")
 * @category Controladores
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Cesar Matias Curiqueo <cmcuriqueo@gmail.com>
 */
class InstitutionalController extends Controller
{

    /**
     * Renderiza
     *
     * @Route("/Institutional", name="saquito_institutional_mostrar")
     *
     * @author Cesar Matias Curiqueo <cmcuriqueo@gmail.com>
     *
     * @category function
     * @return string Código HTML del Link
     */
    public function renderizarAction() {

        $em = $this->getDoctrine()->getManager();
        $institutionals = $em->getRepository('APDSaquitoPaginasBundle:saquitoInstitutional')->findAll();

        return $this->render(
            'APDSaquitoPaginasBundle:Institutional:renderizar.html.twig',
                    array('institutionals' => $institutionals)
        );
    }

}
