<?php

//APD\Saquito\PaginasBundle\Controller\PaginasAdminController.php

namespace APD\Saquito\PaginasBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

/**
 * Acciones adicionales para las páginas en SonataAdmin
 *
 * 
 * @category AdminCoroller
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class PaginasAdminController extends Controller
{

    /**
     * Publica la página en las redes sociales del proyecto
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * @param integer $id Id de la página a publicar
     * @return string Renderiza la página con el resultado si publicó o no la página
     */
    public function publicarPaginaAction($id = null){
        
        $repository = $this->getDoctrine()->getRepository('APDSaquitoPaginasBundle:saquitoPaginas');
        $pagina = $repository->find($id);
        $config = $this->container->getParameter('proyecto');

//        Publicar en cuenta de Facebook
        $facebookeado = $this->container->get('saquitoFacebook')->publicarMensaje(
            'Visitá en el sitio web de ' . $config['titulo'], 
            $pagina->getTitulo(), 
            $config['sitename'] . $this->get('router')->generate('saquito_paginas_ver', array(
                'id' => $pagina->getId(),
                'slug' => $pagina->getSlug())
                ),
            $pagina->getDescripcion(), 
            ''
        );
        
//        Publicar en cuenta de Twitter
        $mensaje = substr($pagina->getTitulo(), 0, 100) . '... - Visitá ésta página en nuestro sitio web ' . $config['sitename']; 
        $twiteado = $this->container->get('saquitoTwitter')->publicarMensaje($mensaje);
        
        return $this->render(
            'APDSaquitoPaginasBundle:Admin:publicado.html.twig', array(
                'pagina' => $pagina,
                'facebookeado' => $facebookeado,
                'twiteado' => $twiteado
                )
        );   
    }
}