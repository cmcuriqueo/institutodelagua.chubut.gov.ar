<?php

namespace APD\Saquito\PaginasBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

//use APD\Saquito\PaginasBundle\Entity\saquitoSliders;

/**
 * Controlador para los Sliders de las portadas
 *
 * @Route("/")
 *
 * @category Controladores
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class SlidersController extends Controller
{

    /**
     * Renderiza el Slider
     *
     * @Route("/slider", name="saquito_slider_mostrar")
     *
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     *
     * @category function
     * @return string Código HTML del slider
     */
    public function renderizarAction() {
        $dispositivo = $this->get('mobile_detect.mobile_detector');

        if ($dispositivo->isMobile()) {
            $aparato = "celular";
        } elseif ($dispositivo->isTablet()) {
                $aparato = "tablet";
            } else {
                $aparato = "desktop";
            }

        $em = $this->getDoctrine()->getManager();
        $sliders = $em->getRepository('APDSaquitoPaginasBundle:saquitoSliders')
                ->findByDispositivo($aparato);

        return $this->render(
            'APDSaquitoPaginasBundle:Sliders:renderizar.html.twig',
                    array('sliders' => $sliders)
        );
    }

}
