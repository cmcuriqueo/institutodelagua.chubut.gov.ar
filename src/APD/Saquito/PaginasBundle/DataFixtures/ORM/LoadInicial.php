<?php

// src/APD/Saquito/PaginasBundle/DataFixtures/ORM/LoadInicial.php

namespace APD\Saquito\PaginasBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use APD\Saquito\PaginasBundle\Entity\saquitoPlantillas;
use APD\Saquito\PaginasBundle\Entity\saquitoPaginas;
use APD\Saquito\PaginasBundle\Entity\saquitoLinks;
use APD\Saquito\PaginasBundle\Entity\saquitoBloques;
use APD\Saquito\PaginasBundle\Entity\saquitoFragmentos;
use APD\Saquito\PaginasBundle\Entity\saquitoMenus;
use APD\Saquito\PaginasBundle\Entity\saquitoInstitutional;
/**
 * Carga los datos de prueba de las Páginas
 *
 *
 * @category Datafixtures
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class LoadInicial extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 3;
    }

    public function load(ObjectManager $manager)
    {

        $yaml = new Parser();

    // Leer el proyecto actual
        $proyecto = $this->container->getParameter('proyecto')['nombre'];


    // Inicializar Menus
        // Cargar datos del Menu de páginas
        $menu = new saquitoMenus;
        $menu->setDescripcion('{raiz}');
        $menu->setPosicion(1000);
        $menu->setSlug('{raiz}');
        $manager->persist($menu);
        $this->addReference('menu-{raiz}', $menu);

        // Tiene que haber siembre un menu oculto, para que la página no aparezca en el menú
        $menu = new saquitoMenus;
        $menu->setDescripcion('{oculto}');
        $menu->setSlug('oculto');
        $manager->persist($menu);
        $this->addReference('menu-{oculto}', $menu);

        // Cargar datos del Menu
        $collecionMenu = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/PaginasBundle/menu.yml'));
        foreach ($collecionMenu['menu'] as $datos) {
            $menu = new saquitoMenus;
            if (!empty($datos['posicion'])) {
                $menu->setPosicion($datos['posicion']);
            } else {
                $menu->setPosicion(0);
            }
            if (!empty($datos['idioma'])) {
                $menu->setIdioma($datos['idioma']);
            } else {
                $menu->setIdioma($this->container->getParameter('proyecto')['idiomapordefecto']);
            }
            if (!empty($datos['descripcion'])) {
                $menu->setDescripcion($datos['descripcion']);
            } else {
                $menu->setDescripcion("{Item}");
            }
            $menu->setSlug($this->container->get('utilesTextos')->slugify($menu->getDescripcion()));

            $manager->persist($menu);
            $this->addReference('menu-'.$datos['descripcion'], $menu);
        }


    // Inicializar Plantillas
        //Primero cargo las plantillas por defecto.
        $collecionPlantillas1 = $yaml->parse(file_get_contents(__DIR__ . '/../../../../Saquito/DataFixtures/PaginasBundle/plantillas.yml'));
        foreach ($collecionPlantillas1['plantillas'] as $datos) {
            $plantilla = new saquitoPlantillas;
            if (!empty($datos['nombre'])) {
                $plantilla->setNombre($datos['nombre']);
            } else {
                $plantilla->setNombre("Nombre de la plantilla");
            }
            if (!empty($datos['descripcion'])) {
                $plantilla->setDescripcion($datos['descripcion']);
            } else {
                $plantilla->setDescripcion("Plantilla:modelo:pagina.html.twig");
            }
            $manager->persist($plantilla);

            $this->addReference('plantilla-'.$datos['descripcion'], $plantilla);
        }
        // Despues cargo las plantillas secundarias
        $collecionPlantillas2 = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/PaginasBundle/plantillas.yml'));
        foreach ($collecionPlantillas2['plantillas'] as $datos) {
            $plantilla = new saquitoPlantillas;
            if (!empty($datos['nombre'])) {
                $plantilla->setNombre($datos['nombre']);
            } else {
                $plantilla->setNombre("Nombre de la plantilla");
            }
            if (!empty($datos['descripcion'])) {
                $plantilla->setDescripcion($datos['descripcion']);
            } else {
                $plantilla->setDescripcion("Plantilla:modelo:pagina.html.twig");
            }
            $manager->persist($plantilla);

            $this->addReference('plantilla-'.$datos['descripcion'], $plantilla);
        }

    // Inicializar Páginas dinámicas
        // Cargar datos de las Páginas dinamicas
        $collecionPaginas = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/PaginasBundle/paginas.yml'));
        foreach ($collecionPaginas['paginas'] as $datos) {
            $pagina = new saquitoPaginas;
            if (!empty($datos['titulo'])) {
                $pagina->setTitulo($datos['titulo']);
            } else {
                $pagina->setTitulo("Título de la página");
            }
            $pagina->setSlug($this->container->get('utilesTextos')->slugify($pagina->getTitulo()));

            if (!empty($datos['descripcion'])) {
                $pagina->setDescripcion($datos['descripcion']);
            }
            if (!empty($datos['keyworks'])) {
                $pagina->setKeyworks($datos['keyworks']);
            }
            if (!empty($datos['robots'])) {
                $pagina->setRobots($datos['robots']);
            }
            if (!empty($datos['autor'])) {
                $pagina->setAutor($datos['autor']);
            } else {
                $pagina->setAutor("APDiseño");
            }
            if (!empty($datos['inicio'])) {
                $pagina->setInicio(new \DateTime($datos['inicio']));
            } else {
                $pagina->setInicio(new \DateTime());
            }
            if (!empty($datos['fin'])) {
                $pagina->setFin(new \DateTime($datos['fin']));
            } else {
                $pagina->setFin(new \DateTime('01-01-2030'));
            }
            if (!empty($datos['publicar'])) {
                $pagina->setPublicar($datos['publicar']);
            } else {
                $pagina->setPublicar(true);
            }
            if (!empty($datos['idioma'])) {
                $pagina->setIdioma($datos['idioma']);
            } else {
                $pagina->setIdioma($this->container->getParameter('proyecto')['idiomapordefecto']);
            }
            if (!empty($datos['menu'])) {
                $pagina->setMenu($this->getReference('menu-' . $datos['menu']));
            } else {
                $pagina->setMenu($this->getReference('menu-{oculto}'));
            }
            if (!empty($datos['plantilla'])) {
                $pagina->setPlantilla($this->getReference('plantilla-' . $datos['plantilla']));
            } else {
                $pagina->setPlantilla();
            }
            $manager->persist($pagina);

            if (!empty($datos['bloques'])) {
                foreach ($datos['bloques'] as $datosBloque) {
                    $bloque = new saquitoBloques;

                    if (!empty($datosBloque['nombre'])) {
                        $bloque->setNombre($datosBloque['nombre']);
                    } else {
                        $bloque->setNombre('Tenés que indicar el nombre del bloque');
                    }

                    if (!empty($datosBloque['html'])) {
                        $bloque->setHtml($datosBloque['html']);
                    } else {
                        $bloque->setHtml('<h2>Felicitaciones!</h2> <p>Ya creaste tu bloque para la página, ahora a modificar el contenido</p>');
                    }

                    $bloque->setPagina($pagina);
                    $manager->persist($bloque);
                }
            }
        }

    // Inicializar Links
        // Cargar datos de los Links
        $collecionLinks = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/PaginasBundle/links.yml'));
        foreach ($collecionLinks['links'] as $datos) {
            $link = new saquitoLinks;
            if (!empty($datos['titulo'])) {
                $link->setTitulo($datos['titulo']);
            } else {
                $link->setTitulo("Título del link");
            }
            $link->setSlug($this->container->get('utilesTextos')->slugify($link->getTitulo()));

            if (!empty($datos['idioma'])) {
                $link->setIdioma($datos['idioma']);
            } else {
                $link->setIdioma($this->container->getParameter('proyecto')['idiomapordefecto']);
            }
            if (!empty($datos['url'])) {
                $link->setUrl($datos['url']);
            } else {
                $link->setUrl("#");
            }
            if (!empty($datos['menu'])) {
                $link->setMenu($this->getReference('menu-' . $datos['menu']));
            } else {
                $link->setMenu($this->getReference('menu-{oculto}'));
            }
            $manager->persist($link);

        }

    // Inicializar Fragmentos
        // Cargar datos de los fragmentos de páginas
        $collecionFragmentos = $yaml->parse(file_get_contents(__DIR__ . '/../../../../'. $proyecto .'/DataFixtures/PaginasBundle/fragmentos.yml'));
        foreach ($collecionFragmentos['fragmentos'] as $datos) {
            $fragmento = new saquitoFragmentos;
            if (!empty($datos['nombre'])) {
                $fragmento->setNombre($datos['nombre']);
            } else {
                $fragmento->setNombre('nombre');
            }
            if (!empty($datos['contenido'])) {
                $fragmento->setContenido($datos['contenido']);
            } else {
                $fragmento->setContenido('[Agregar contenido]');
            }
            $fragmento->setSlug($this->container->get('utilesTextos')->slugify($fragmento->getNombre()));

            $manager->persist($fragmento);
        }

        $manager->flush();
    }

}
