<?php

namespace APD\Saquito\PaginasBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Paquete encargado de la administración de Páginas HTML
 *
 * <strong>Objetivo:</strong> Un sitio o aplicación web, está conformado por un conjunto de páginas con características similares. 
 * Este Bundle crea un módulo para que el administrador de contenido pueda crear, editar y borrar páginas con 
 * plantillas pre definidas. Las plantillas se crean en la programación del proyecto y pueden tener varios Bloques 
 * de contenidos editables.-
 * 
 * @category Bundle
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class APDSaquitoPaginasBundle extends Bundle
{
}
