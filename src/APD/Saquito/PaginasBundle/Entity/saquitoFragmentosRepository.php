<?php

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Repositorio de la entidad saquitoFragmenos
 *
 * 
 * @category Repositorio
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoFragmentosRepository extends EntityRepository
{
    
}
