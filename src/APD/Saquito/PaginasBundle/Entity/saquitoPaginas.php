<?php

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Entidad encargada de el manejo de los 'Paginas'
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="APD\Saquito\PaginasBundle\Entity\saquitoPaginasRepository")
 *
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoPaginas
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $titulo
     *
     * @ORM\Column(name="titulo", type="string", length=150, nullable=false)
     */
    private $titulo;

    /**
     * @var string $slug
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @var string $keyworks
     *
     * @ORM\Column(name="keyworks", type="string", length=255, nullable=true)
     */
    private $keyworks;

    /**
     * @var string $descripcion
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @var string $robots
     *
     * @ORM\Column(name="robots", type="string", length=150, nullable=true)
     */
    private $robots;

    /**
     * @var string $autor
     *
     * @ORM\Column(name="autor", type="string", length=150, nullable=true, options={"default": "APD Diseño"})
     */
    private $autor;

    /**
     * @var string $idioma
     *
     * @ORM\Column(name="idioma", type="string", length=10, nullable=true, options={"default": "es"})
     */
    private $idioma;

    /**
     * @var \DateTime $inicio
     *
     * @ORM\Column(name="inicio", type="date")
     */
    private $inicio;

    /**
     * @var \DateTime $fin
     *
     * @ORM\Column(name="fin", type="date")
     */
    private $fin;


    /**
     * @var boolean $publicar
     *
     * @ORM\Column(name="publicar", type="boolean", nullable=true)
     */
    private $publicar;

    /**
     * @var boolean
     *
     * @ORM\Column(name="privado", type="boolean", nullable=true)
     */
    private $privado;

    /**
     * @ORM\ManyToOne(targetEntity="saquitoPlantillas", inversedBy="paginas")
     * @ORM\JoinColumn(name="planilla_id", referencedColumnName = "id")
     */
    protected $plantilla;

    /**
     * @ORM\ManyToOne(targetEntity="APD\Saquito\PaginasBundle\Entity\saquitoMenus", inversedBy="paginas")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName = "id")
     */
    protected $menu;

    /**
     * @ORM\OneToMany(targetEntity="saquitoBloques", mappedBy="pagina", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"nombre" = "DESC"})
     *
     */
    protected $bloques;

    /**
     * Add bloques
     *
     * @param saquitoBloques $bloques
     * @return saquitoPaginas
     */
    public function addBloques(saquitoBloques $bloques)
    {
        $this->bloques[] = $bloques;
        return $this;
    }

    /**
     * Remove bloque
     *
     * @param saquitoBloques $bloque
     */
    public function removeBloques(saquitoBloques $bloques)
    {
        $this->bloques->removeElement($bloques);
    }

    /**
     * Get bloques
     *
     */
    public function getBloques()
    {
        return $this->bloques;
    }

    public function setBloques($bloques)
    {
        $this->bloques = $bloques;
    }

    public function __construct() {
        $this->bloques = new ArrayCollection();
        $this->setPrivado(false);

    }

    /**
     * Set planilla
     *
     * @param APD\Saquito\PaginasBundle\Entity\saquitoPlantillas $plantilla
     * @return saquitoPaginas
     */
    public function setPlantilla(\APD\Saquito\PaginasBundle\Entity\saquitoPlantillas $plantilla = null)
    {
        $this->plantilla = $plantilla;
        return $this;
    }

    /**
     * Get plantilla
     *
     * @return APD\Saquito\PaginasBundle\Entity\saquitoPlantillas
     */
    public function getPlantilla()
    {
        return $this->plantilla;
    }

    /**
     * Set menu
     *
     * @param APD\Saquito\PaginasBundle\Entity\saquitoMenus $menu
     * @return saquitoPaginas
     */
    public function setMenu(\APD\Saquito\PaginasBundle\Entity\saquitoMenus $menu = null)
    {
        $this->menu = $menu;
        return $this;
    }

    /**
     * Get menu
     *
     * @return APD\Saquito\PaginasBundle\Entity\saquitoMenus
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return saquitoPaginas
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set idioma
     *
     * @param string $idioma
     * @return saquitoPaginas
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;
        return $this;
    }

    /**
     * Get idioma
     *
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set keyworks
     *
     * @param string $keyworks
     * @return saquitoPaginas
     */
    public function setKeyworks($keyworks)
    {
        $this->keyworks = $keyworks;

        return $this;
    }

    /**
     * Get keyworks
     *
     * @return string
     */
    public function getKeyworks()
    {
        return $this->keyworks;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return saquitoPaginas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set robots
     *
     * @param string $robots
     * @return saquitoPaginas
     */
    public function setRobots($robots)
    {
        $this->robots = $robots;

        return $this;
    }

    /**
     * Get robots
     *
     * @return string
     */
    public function getRobots()
    {
        return $this->robots;
    }

    /**
     * Set autor
     *
     * @param string $autor
     * @return saquitoPaginas
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;

        return $this;
    }

    /**
     * Get autor
     *
     * @return string
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     * @return saquitoPaginas
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;

        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     * @return saquitoPaginas
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set publicar
     *
     * @param boolean $publicar
     * @return saquitoPaginas
     */
    public function setPublicar($publicar)
    {
        $this->publicar = $publicar;

        return $this;
    }

    /**
     * Set privado
     *
     * @param boolean $privado
     * @return saquitoPaginas
     */
    public function setPrivado($privado)
    {
        $this->privado = $privado;

        return $this;
    }

    /**
     * Get privado
     *
     * @return boolean
     */
    public function getPrivado()
    {
        return $this->privado;
    }

    /**
     * Get activa
     *
     * @return boolean
     */
    public function getActiva() {
        if ($this->getPublicar() ) {
            $actual = new \DateTime("now");
            if (($actual >= $this->getInicio()) && ($actual <= $this->getFin())) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    /**
     * Get publicar
     *
     * @return boolean
     */
    public function getPublicar()
    {
        return $this->publicar;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @return saquitoPaginas
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function __toString() {
        return $this->titulo;
    }
}
