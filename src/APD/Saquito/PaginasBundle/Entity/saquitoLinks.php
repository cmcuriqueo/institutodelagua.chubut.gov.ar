<?php
// APD\Saquito\PaginasBundle\Entity\saquitoLinks

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entidad encargada de el manejo de los 'Links'
 *
 * @ORM\Table()
 * @ORM\Entity()
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoLinks
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Tenés que ingresar un título")
     */
    private $titulo;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @var string $idioma
     * 
     * @ORM\Column(name="idioma", type="string", length=10, nullable=true, options={"default": "es"})
     */
    private $idioma;
    
    /**
     * @ORM\ManyToOne(targetEntity="APD\Saquito\PaginasBundle\Entity\saquitoMenus", inversedBy="links")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName = "id")
     */
    protected $menu;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return saquitoLinks
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return saquitoLinks
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * Set menu
     *
     * @param APD\Saquito\PaginasBundle\Entity\saquitoMenus $menu
     * @return saquitoLinks
     */
    public function setMenu(\APD\Saquito\PaginasBundle\Entity\saquitoMenus $menu = null)
    {
        $this->menu = $menu;
        return $this;
    }

    /**
     * Get menu
     *
     * @return APD\Saquito\PaginasBundle\Entity\saquitoMenus
     */
    public function getMenu()
    {
        return $this->menu;
    }          

    /**
     * Set idioma
     *
     * @param string $idioma
     * @return saquitoPaginas
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;
        return $this;
    }

    /**
     * Get idioma
     *
     * @return string 
     */
    public function getIdioma()
    {
        return $this->idioma;
    }
    
}
