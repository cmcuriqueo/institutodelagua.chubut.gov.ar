<?php

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Repositorio de la entidad saquitoMenus
 *
 * 
 * @category Repositorio
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoMenusRepository extends EntityRepository
{
    /**
     * Buscar todos los menus ordenado por Posicionn
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return array Devuelve un array de resultados
     */
    public function buscarTodos($idioma)
    {
        return $this
            ->createQueryBuilder('p')
            ->select('p')
            ->where('p.idioma = :idioma')->setParameter('idioma', $idioma)
            ->orWhere('p.descripcion = :descr')->setParameter('descr','{raiz}')
            ->andWhere('p.publicar = true')
            ->orderBy('p.posicion', 'ASC')
            ->getQuery()
            ->getResult()
        ;        
    }
}
