<?php

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Entidad encargada de el manejo de los 'Menus'
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks 
 * @ORM\Entity(repositoryClass="APD\Saquito\PaginasBundle\Entity\saquitoMenusRepository")
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoMenus
{
    /**
     * @var integer
     * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     */
    private $descripcion;

    
    /**
     * @var string $idioma
     * 
     * @ORM\Column(name="idioma", type="string", length=10, nullable=true, options={"default": "es"})
     */
    private $idioma;

    /**
     * @var integer
     * 
     * @ORM\Column(name="posicion", type="integer", nullable=true)
     */
    private $posicion;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="APD\Saquito\PaginasBundle\Entity\saquitoPaginas", mappedBy="menu")
     */
    protected $paginas;

    /**
     * @ORM\OneToMany(targetEntity="APD\Saquito\PaginasBundle\Entity\saquitoLinks", mappedBy="menu")
     */
    protected $links;

    /**
     * @var boolean
     *
     * @ORM\Column(name="privado", type="boolean", nullable=true)
     */
    private $privado;
    
    /**
     * @var boolean $publicar
     * 
     * @ORM\Column(name="publicar", type="boolean", nullable=true)
     */
    private $publicar;
    
    /**
     * Get paginas
     *
     */
    public function getPaginas()
    {
        return $this->paginas;
    }

    /**
     * Get links
     *
     */
    public function getLinks()
    {
        return $this->links;
    }
    
    public function __construct() {

        $this->paginas = new ArrayCollection();
        $this->links = new ArrayCollection();
        $this->setPrivado(false);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idioma
     *
     * @param string $idioma
     * @return saquitoMenus
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;
        return $this;
    }

    /**
     * Get idioma
     *
     * @return string 
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return saquitoMenus
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Set posicion
     *
     * @param integer $posicion
     * @return saquitoMenus
     */
    public function setPosicion($posicion)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return integer 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Set privado
     *
     * @param boolean $privado
     * @return saquitoMenus
     */
    public function setPrivado($privado)
    {
        $this->privado = $privado;

        return $this;
    }

    /**
     * Get privado
     *
     * @return boolean 
     */
    public function getPrivado()
    {
        return $this->privado;
    }    
    
    /**
     * Set publicar
     *
     * @param boolean $publicar
     * @return saquitoPaginas
     */
    public function setPublicar($publicar)
    {
        $this->publicar = $publicar;

        return $this;
    }

    /**
     * Get publicar
     *
     * @return boolean 
     */
    public function getPublicar()
    {
        return $this->publicar;
    }

    
    public function __toString() {
        return $this->descripcion;
    }
    
}
