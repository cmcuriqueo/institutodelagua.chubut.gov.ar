<?php

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entidad encargada de el manejo de los 'ParallaxLinks'
 *
 * @ORM\Table()
 * @ORM\Entity()
 *
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Cesar Matias Curiqueo <cmcuriqueo@gmail.com>
 */
class saquitoInstitutional
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="acronym", type="string", length=100, nullable=true)
     */
    protected $acronym;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=200, nullable=false)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="button", type="string", length=40, nullable=false)
     */
    protected $button;


    /**
     * Set target
     *
     * @param integer $id
     * @return saquitoInstitutional
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set full_name
     *
     * @param string $name
     * @return saquitoInstitutional
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set abbreviation
     *
     * @param string $acronym
     * @return saquitoInstitutional
     */
    public function setAcronym($acronym)
    {
        $this->acronym = $acronym;
        return $this;
    }

    /**
     * Get acronym
     *
     * @return string
     */
    public function getAcronym()
    {
        return $this->acronym;
    }

    /**
     * Set link
     *
     * @param string $url
     * @return saquitoInstitutional
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set button
     *
     * @param string $button
     * @return saquitoInstitutional
     */
    public function setButton($button)
    {
        $this->button = $button;
        return $this;
    }

    /**
     * Get button
     *
     * @return string
     */
    public function getButton()
    {
        return $this->button;
    }
}
