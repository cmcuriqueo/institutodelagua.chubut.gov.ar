<?php

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entidad encargada de el manejo de los 'Sliders'
 *
 * @ORM\Table()
 * @ORM\Entity()
 *
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoSliders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="target", type="string", length=100, nullable=true)
     */
    private $target;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

//    /**
//     * @var string
//     *
//     * @ORM\Column(name="html", type="string", length=255, nullable=true)
//     */
//    private $html;


    /**
     * @ORM\Column(type="string", length=20)
     */
    private $dispositivo;

    /**
     * @ORM\Column(name="button", type="string", length=50, nullable=true)
     */
    private $button;


    /**
     * Set id
     *
     * @param string $id
     * @return saquitoSliders
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set target
     *
     * @param string $target
     * @return saquitoSliders
     */
    public function setTarget($target)
    {
        $this->target = $target;
        return $this;
    }

    /**
     * Get target
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set button
     *
     * @param string $button
     * @return saquitoSliders
     */
    public function setButton($button)
    {
        $this->button = $button;
        return $this;
    }

    /**
     * Get button
     *
     * @return string
     */
    public function getButton()
    {
        return $this->button;
    }

    /**
     * Set dispositivo
     *
     * @param string $dispositivo
     * @return saquitoSliders
     */
    public function setDispositivo($dispositivo)
    {
        $this->dispositivo = $dispositivo;
        return $this;
    }

    /**
     * Get dispositivo
     *
     * @return string
     */
    public function getDispositivo()
    {
        return $this->dispositivo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return saquitoSliders
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return saquitoSliders
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

//    /**
//     * Set html
//     *
//     * @param string $html
//     * @return saquitoSliders
//     */
//    public function setHtml($html)
//    {
//        $this->html = $html;
//
//        return $this;
//    }
//
//    /**
//     * Get html
//     *
//     * @return string
//     */
//    public function getHtml()
//    {
//        return $this->html;
//    }

    public function __toString() {
        return $this->descripcion;
    }

 // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * @Assert\File
     */
    private $archivo;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * Set path
     *
     * @param string $path
     * @return saquitoSliders
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set archivo
     *
     * @param UploadedFile $archivo
     */
    public function setArchivo(UploadedFile $archivo = null)
    {
        $this->archivo = $archivo;
    }

    public function getVerSlyder() {
//         Le saco la carpeta pública (public_html, web, o lo que sea...) a path, en este caso no tiene que estar.
        return null === $this->getPath()
            ? null
            : substr($this->getPath(), strpos($this->getPath(), '/')) . $this->getUploadDir() . '/' . $this->getId() . '.jpg';
    }

    /**
     * Get archivo
     *
     * @return UploadedFile
     */
    public function getArchivo()
    {
        return $this->archivo;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../../' . $this->getPath() . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return '/hot/sliders';
    }

    /**
     * @ORM\PreRemove()
     */
    public function removeUpload()
    {
        $file = $this->getUploadRootDir().'/' . $this->getId().'.jpg';
        if (file_exists($file)) {
            unlink($file);
        }
    }

    public function upload()
    {
        if (null === $this->archivo) {
            return;
        }
        $this->archivo->move( $this->getUploadRootDir(),  $this->getId().'.jpg');
        $this->archivo = null;
    }
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


}
