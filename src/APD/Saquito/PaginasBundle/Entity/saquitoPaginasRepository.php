<?php

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\EntityRepository;
use APD\Saquito\MainBundle\Entity\saquitoResultados;


/**
 * Repositorio de la entidad saquitoPaginas
 *
 * 
 * @category Repositorio
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoPaginasRepository extends EntityRepository
{

    /**
     * Buscar todas las páginas
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return array Devuelve un array de resultados ordenado por título
     */      
    public function buscarTodos()
    {
        return $this
            ->createQueryBuilder('p')
            ->select('p')
//            ->where('p.fecha <= :now')->setParameter('now', new \DateTime())
            ->orderBy('p.titulo', 'DESC')
            ->getQuery()
            ->getResult()
        ;        
    }

    /**
     * Busca una página por Slug
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @return array Devuelve la página o null si no lo encuentra
     */      
    public function findBySlug($slug)
    {
        $qb = $this->createQueryBuilder('p')
                   ->addSelect('p')
                   ->where('p.slug = :slug')
                   ->setParameter('slug', $slug)
                   ;

        $query = $qb->getQuery();

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e){
            return null;
        }
    } 
    
    /**
     * Buscar dentro de las páginas un texto en particular
     * 
     * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
     * 
     * @category function
     * 
     * @param string $cadena Cadena a buscar en las páginas
     * @return array Devuelve un array de resultados ordenado por fecha de inicio
     */        
    public function buscar($permisos,$cadena)
    {
        $resultados = [];
         
        $qb = $this->createQueryBuilder('p')
                    ->orderBy('p.titulo', 'DESC')
//                    ->setMaxResults( 10 )
                    ->where('p.titulo LIKE :cadena')
                    ->orWhere('p.keyworks LIKE :cadena')
                    ->orWhere('p.descripcion LIKE :cadena')
                    ->setParameter('cadena', '%' . $cadena . '%')
                ;

        $query = $qb->getQuery();
        try {
            $lista = $query->getResult();
            foreach ($lista as $val) {
                $item = new saquitoResultados();
                $item->setTitulo($val->getTitulo());
                
                $item->setDescripcion('<strong>[PÁGINAS] </strong> ' . $val->getDescripcion());
//                $item->setFecha(new \DateTime());
                $item->setEntidad("saquitoPaginas");
                $item->setLink("/es/". $val->getId() ."/".$val->getSlug());
                array_push($resultados, $item);
            }
            return $resultados;
        } catch (\Doctrine\ORM\NoResultException $e){
            return null;
        }
    }        
    
}
