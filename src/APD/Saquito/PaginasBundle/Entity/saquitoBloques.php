<?php

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entidad encargada de el manejo de los 'Bloques'
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks 
 * @ORM\Entity(repositoryClass="APD\Saquito\PaginasBundle\Entity\saquitoBloquesRepository")
 * 
 * @category Entidades
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoBloques
{
    /**
     * @var integer $id
     * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     * 
     * @ORM\Column(name="nombre", type="string", length=150, nullable=false)
     */
    private $nombre;

    /**
     * @var string $html
     * 
     * @ORM\Column(name="html", type="text")
     */
    private $html;

    /**
     * @ORM\ManyToOne(targetEntity="saquitoPaginas", inversedBy="bloques", cascade={"persist"})
     * @ORM\JoinColumn(name="pagina_id", referencedColumnName = "id")
     */
    protected $pagina;
  
    /**
     * Set pagina
     *
     * @param APD\Saquito\PaginasBundle\Entity\saquitoPaginas $pagina
     * @return saquitoBloques
     */
    public function setPagina(\APD\Saquito\PaginasBundle\Entity\saquitoPaginas $pagina = null)
    {
        $this->pagina = $pagina;
        return $this;
    }

    /**
     * Get pagina
     *
     * @return APD\Saquito\PaginasBundle\Entity\saquitoPaginas
     */
    public function getPagina()
    {
        return $this->pagina;
    }        
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return saquitoBloques
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set html
     *
     * @param string $html
     * @return saquitoBloques
     */
    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Get html
     *
     * @return string 
     */
    public function getHtml()
    {
        return $this->html;
    }
}
