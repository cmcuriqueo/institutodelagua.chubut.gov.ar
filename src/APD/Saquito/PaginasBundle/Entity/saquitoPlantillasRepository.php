<?php

namespace APD\Saquito\PaginasBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Repositorio de la entidad saquitoPlantillas
 *
 * 
 * @category Repositorio
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class saquitoPlantillasRepository extends EntityRepository
{
}
