<?php
namespace APD\Saquito\PaginasBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
//use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
//use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clase para administrar los Sliders (SonataAdmin)
 *
 *
 * @category Admin
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class SliderAdmin extends Admin
{
//    public $supportsPreviewMode = true;
    protected $baseRouteName = 'saquitoslider_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'descripcion' // field name
        );

    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    private $container;

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $sizes = array();
        for ($index = 35; $index <= 48; $index++) {
            $sizes[$index] = $index . "";
        }


        $targets = ["_blank"=>"Nueva pestaña","_parent"=>"Misma pestaña"];

        $dispositivo = array("celular"=>"Celulares",
                    "tablet"=>"Tablets",
                    "desktop"=>"Computadoras de escritorio");

        $formMapper
            ->add('descripcion',null,array('attr'=>array('style'=>'width:800px')))
            ->add('link',null,array('attr'=>array('style'=>'width:800px')))
            ->add('dispositivo','choice', array('required' => true, 'expanded' => false, 'multiple' => false, 'choices' => $dispositivo))
            ->add('target','choice', array('required' => true, 'expanded' => false, 'multiple' => false, 'choices' => $targets))
            ->add('archivo','file', array(
                'required'    => false,
                'label'       => 'Archivo'
            ))
            ->add('button', null, array('attr'=>array('style'=>'width:280px', 'placeholder' => 'Ejemplo: +INFORMACIÓN')))

        ;
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setButton('+INFORMACIÓN');
        $instance->setDescripcion('<br><br><br><br><br><br><br><br><br><br>');

        return $instance;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('descripcion')
            ->add('target')
            ->add('link');
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('descripcion')
            ->add('dispositivo')
            ->add('target')
            ->add('link');
    }

    private function guardarPath ($entidad) {
        $proyecto = $this->container->getParameter('proyecto');
        $entidad->setPath($proyecto['carpeta_publica']. '/' . $proyecto['nombre']);
    }

    public function prePersist($entidad) {
      $this->guardarPath($entidad);
    }

    public function preUpdate($entidad) {
      $this->guardarPath($entidad);
    }

    public function postPersist($entidad) {
      $this->subirArchivo($entidad);
    }

    public function postUpdate($entidad) {
        $this->subirArchivo($entidad);
    }

    public function subirArchivo($entidad) {
        $entidad->upload();
    }
}
