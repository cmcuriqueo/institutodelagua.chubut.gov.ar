<?php
namespace APD\Saquito\PaginasBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clase para administrar los Bloques (SonataAdmin)
 *
 * 
 * @category Admin
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class BloquesAdmin extends Admin
{
    protected $baseRouteName = 'saquitobloques_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'nombre' // field name
        );    
   
    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    private $container;

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Bloque')
                ->add('nombre',null,array('attr'=>array('style'=>'width:100%')))
                ->add('html', 'ckeditor', array())
            ->end() 
         
        ;
        
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nombre')
//            ->add('seccion')
//            ->add('plantilla')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nombre')
            ->addIdentifier('pagina')
//            ->add('seccion')
//            ->add('_action', 'Acciones', array(
//                    'actions' => array(
//                    'edit' => array(),
//                    'delete' => array('label' =>''),
//                    'fotoportada' => array('template' => 'NewsBundle:Admin:list__action_fotoportada.html.twig'),
//                    'publicar' => array('template' => 'NewsBundle:Admin:list__action_publicar.html.twig'),
//            )))
            ;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('nombre')
                ->assertMaxLength(array('limit' => 150))
            ->end()
//            ->with('copete')
//                ->assertMaxLength(array('limit' => 255))
//            ->end()
//            ->with('volanta')
//                ->assertMaxLength(array('limit' => 50))
//            ->end()
        ;
    }

    public function postRemove($entidad) {

        }

    public function postUpdate($entidad) {

        }
    
    public function postPersist($entidad) {

        }

    
    protected function configureRoutes(RouteCollection $collection) {

        }
}