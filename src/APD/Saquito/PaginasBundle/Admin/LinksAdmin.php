<?php
namespace APD\Saquito\PaginasBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clase para administrar las Links (SonataAdmin)
 *
 * 
 * @category Admin
 * @package Saquito
 * @copyright (c) 2016, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class LinksAdmin extends Admin
{
    public $supportsPreviewMode = true;
    protected $baseRouteName = 'saquitolinks_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'nombre' // field name
        );    
   
    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    private $container;

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        
        $formMapper
            ->add('titulo', null,
                array(
                ))
            ->add('url', null,
                array(
                ))
            ->add('idioma','choice',
                array(
                    'choices' => $this->container->getParameter('proyecto')['idiomas']
                ))                
            ->add('menu', 'entity', 
                array(
                    'class' => 'APDSaquitoPaginasBundle:saquitoMenus',
                    'property' => 'descripcion',
                ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('titulo')
            ->add('idioma')
            ->add('menu')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titulo')
            ->add('idioma')
            ->add('menu')
            ->add('url')
            ;
    }
    
    public function prePersist($link) {
        $link->setSlug($this->container->get('utilesTextos')->slugify($link->getTitulo()));
    }

    public function preUpdate($link) {
        $link->setSlug($this->container->get('utilesTextos')->slugify($link->getTitulo()));
    }

    
}