<?php
namespace APD\Saquito\PaginasBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
//use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
//use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clase para administrar los Links Intitucionales (SonataAdmin)
 *
 *
 * @category Admin
 * @package Saquito
 * @copyright (c) 2018, APDiseño
 * @author cesar matias curiqueo <cmcuriqueo@gmail.com>
 */
class InstitutionalAdmin extends Admin
{

    protected $baseRouteName = 'saquitoinstitutional_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'full_name_institution' // field name
        );
        protected $em;

        public function setEntityManager(EntityManager $em)
        {
            $this->em = $em;
        }
        private $container;

        public function setContainer(ContainerInterface $container){
            $this->container = $container;
        }

        protected function configureFormFields(FormMapper $formMapper)
        {
            $sizes = array();
            for ($index = 35; $index <= 48; $index++) {
                $sizes[$index] = $index . "";
            }

            $formMapper
                ->add('acronym',null,array('attr'=>array('style'=>'width:200px', 'placeholder' => 'Ejemplo: IPA')))
                ->add('name',null,array('attr'=>array( 'placeholder' => 'Ejemplo: Instituto Provincial del Agua')))
                ->add('url',null, array('required' => true, 'attr'=> array('placeholder' => 'Web Site' )))
                ->add('button', null, array('required' => true, 'attr'=> array('placeholder' => 'Ejemplo: Ir al sitio web' )));
        }

        public function getNewInstance()
        {
            $instance = parent::getNewInstance();
            $instance->setButton('Ir al sitio web');

            return $instance;
        }

        protected function configureDatagridFilters(DatagridMapper $datagridMapper)
        {
            $datagridMapper
                ->add('acronym')
                ->add('name')
                ->add('url')
            ;
        }

        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->addIdentifier('id')
                ->add('acronym')
                ->addIdentifier('name')
                ->add('url');
        }
}
