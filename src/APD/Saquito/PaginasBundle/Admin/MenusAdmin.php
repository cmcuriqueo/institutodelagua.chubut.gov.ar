<?php
namespace APD\Saquito\PaginasBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clase para administrar los Menus (SonataAdmin)
 *
 * 
 * @category Admin
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class MenusAdmin extends Admin
{
    protected $baseRouteName = 'saquitomenus_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', 
        '_sort_by' => 'descripcion' 
        );    
   
    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    private $container;

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('descripcion',null,array())
            ->add('posicion',null,array())
            ->add('publicar',null,array())
            ->add('privado',null, 
                array(
                    'label' => 'USO INTERNO: '
                ))    
            ->add('idioma','choice',
                array(
                    'choices' => $this->container->getParameter('proyecto')['idiomas']
                ))
        ;
        
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('descripcion')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('descripcion')
            ->add('privado',null, 
                array(
                    'label' => 'USO INTERNO: '
                ))    
            ->add('publicar')
            ->add('idioma')
            ;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
//        $errorElement
//            ->with('descripcion')
//                ->assertMaxLength(array('limit' => 150))
//            ->end()
//        ;
    }

    public function prePersist($menu) {
        $menu->setSlug($this->container->get('utilesTextos')->slugify($menu->getDescripcion()));
    }

    public function preUpdate($menu) {
        $menu->setSlug($this->container->get('utilesTextos')->slugify($menu->getDescripcion()));
    }
    
    
}