<?php
namespace APD\Saquito\PaginasBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clase para administrar los fragmentos (SonataAdmin)
 *
 * 
 * @category Admin
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class FragmentosAdmin extends Admin
{
    protected $baseRouteName = 'saquitofragmentos_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', 
        '_sort_by' => 'nombre' 
        );    
   
    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    private $container;

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nombre',null,array())
            ->add('contenido', 'ckeditor', array(
                'config' => array(
                    'filebrowser_image_browse_url' => array(
                        'route'            => 'elfinder',
                        'route_parameters' => array('instance' => 'ckeditor'),
                    ),
                ),
            ))
        ;
        
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nombre')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nombre')
            ;
    }
    public function prePersist($fragmento) {
        $fragmento->setSlug($this->container->get('utilesTextos')->slugify($fragmento->getNombre()));
    }

    public function preUpdate($fragmento) {
        $fragmento->setSlug($this->container->get('utilesTextos')->slugify($fragmento->getNombre()));
    }
}