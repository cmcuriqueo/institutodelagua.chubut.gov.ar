<?php
namespace APD\Saquito\PaginasBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
//use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clase para administrar las Páginas (SonataAdmin)
 *
 *
 * @category Admin
 * @package Saquito
 * @copyright (c) 2015, APDiseño
 * @author Andrés Pieruccioni <andrespieruccioni@gmail.com>
 */
class PaginasAdmin extends Admin
{
    public $supportsPreviewMode = true;
    protected $baseRouteName = 'saquitopaginas_admin';
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'titulo' // field name
        );

    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    private $container;

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->with('Información de la página')
                ->add('titulo', null,
                    array(
                    ))
                ->add('menu', 'entity',
                    array(
                        'class' => 'APDSaquitoPaginasBundle:saquitoMenus',
                        'property' => 'descripcion',
                        'attr'=> array('style'=>'width:225px')
                    ))
                ->add('idioma','choice',
                    array(
                        'choices' => $this->container->getParameter('proyecto')['idiomas'],
                        'attr'=> array('style'=>'width:225px')
                    ))
                ->add('plantilla', 'entity', array(
                    'class' => 'APDSaquitoPaginasBundle:saquitoPlantillas',
                    'property' => 'nombre',
                ))
            ->end()
            ->with('Publicación de la página')
                ->add('privado',null,
                    array(
                        'label' => 'USO INTERNO: '
                    ))
                ->add('publicar',null,
                    array(
                        'label' => 'Publicar en la Web',
                        'required'  => false
                    ))
                ->add('inicio', 'sonata_type_date_picker', array(
                        'format'                => 'dd/M/yyyy',
                        'dp_side_by_side'       => true,
                ))
                ->add('fin', 'sonata_type_date_picker', array(
                        'format'                => 'dd/M/yyyy',
                        'dp_side_by_side'       => false,
                ))
            ->end()
            ->with('Meta etiquetas')
                ->add('keyworks',null,
                    array(
                    ))
                ->add('descripcion',null,
                    array(
                    ))
                ->add('robots',null,
                    array(
                    ))
                ->add('autor',null,
                    array(
                    ))
            ->end()

            ->with('Bloques de contenido',array('collapsed'=>false))
                ->add('bloques', 'sonata_type_collection',
                    array(
                        'required' => false,
//                        'expanded' => true,
                        'by_reference' => true,
    //                    'type_options' => array('delete' => false)
                    ), array(
                        'edit' => 'inline',
                    ))
            ->end()

        ;
    }



    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('titulo')
            ->add('menu')
            ->add('privado', null,
                    array( 'label' => 'Uso Interno'
                    ))
            ->add('idioma')
            ->add('plantilla')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titulo')
            ->add('idioma')
            ->add('menu')
            ->add('plantilla')
            ->add('privado', null,
                    array( 'label' => 'Uso Interno'
                    ))
            ->add('_action', 'Acciones', array(
                    'actions' => array(
                        'publicar' => array('template' => 'APDSaquitoPaginasBundle:Admin:list__action_publicar.html.twig'),
                    )))
            ;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'preview':
                return 'APDSaquitoPaginasBundle:Plantillas:vistapreliminar.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
//    public function validate(ErrorElement $errorElement, $object)
//    {
//        $errorElement
//            ->with('titulo')
//                ->assertMaxLength(array('limit' => 150))
//            ->end()
////            ->with('copete')
////                ->assertMaxLength(array('limit' => 255))
////            ->end()
////            ->with('volanta')
////                ->assertMaxLength(array('limit' => 50))
////            ->end()
//        ;
//    }

//    private function controlCambios($entidad,$accion) {
//        $log = new baseLog;
//        $log->setUsuario($this->container->get('security.context')->getToken()->getUser()->getUsername());
//        $log->setIp($this->getRequest()->getClientIp());
//        $log->setFecha(new \DateTime());
//        $log->setTabla("newsNoticias");
//        $log->setRegistro( $entidad->getId());
//        $log->setAccion($accion);
//        $this->em->persist($log);
//        $this->em->flush();
//    }

    public function prePersist($pagina) {
        $pagina->setSlug($this->container->get('utilesTextos')->slugify($pagina->getTitulo()));
        foreach ($pagina->getBloques() as $bloque) {
            $bloque->setPagina($pagina);
        }
    }

    public function preUpdate($pagina) {
        $pagina->setSlug($this->container->get('utilesTextos')->slugify($pagina->getTitulo()));
        foreach ($pagina->getBloques() as $bloque) {
            $bloque->setPagina($pagina);
        }
        $pagina->setBloques($pagina->getBloques());
    }

    public function postRemove($entidad) {
//        $this->controlCambios($entidad,'Baja');
    }

    public function postUpdate($entidad) {
//        $this->controlCambios($entidad,'Modificación');
    }

    public function postPersist($entidad) {
//        $this->controlCambios($entidad,'Alta');
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('publicarpagina',$this->getRouterIdParameter().'/publicarpagina');
    }
}
