<?php
// src/APD/Saquito/PaginasBundle/Menu/Builder.php
namespace APD\Saquito\PaginasBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;


class Construir extends ContainerAware
{
    public function menuPaginas(FactoryInterface $factory, array $options) {
        $em = $this->container->get('doctrine.orm.entity_manager');
        
        
        $menus = $em->getRepository('APDSaquitoPaginasBundle:saquitoMenus')->buscarTodos('es');

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');
        foreach ($menus as $item) {
            // Si la descripción del menu es distinto de root y oculto, creo el menú
            if ($item->getDescripcion()  <> '{oculto}') {
                if ($item->getDescripcion() == '{raiz}') {
                    foreach ($item->getPaginas() as $pagina) {
                        if ($pagina->getActiva()){
                            $menu->addChild($pagina->getTitulo(), 
                                    array('uri' => 
                                        $this->container->get('router')->generate('saquito_paginas_ver', array(
                                            'id' => $pagina->getId(),
                                            'slug' => $pagina->getSlug()
                                        )
                                    )));
                        }
                    }
                } else {
                    $menu->addChild($item->getDescripcion())
                         ->setAttribute('dropdown', true);
                    foreach ($item->getPaginas() as $pagina) {
                        if ($pagina->getActiva()){
                            $menu[$item->getDescripcion()]->addChild($pagina->getTitulo(), 
                                    array('uri' => 
                                        $this->container->get('router')->generate('saquito_paginas_ver', array(
                                            'id' => $pagina->getId(),
                                            'slug' => $pagina->getSlug()
                                        )
                                    )));
                        }
                    }
                }
            }
        }
        return $menu;
    }
}

































