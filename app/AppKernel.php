<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
//  -----------------------------------------------------
    protected $proyecto;

    public function __construct($environment, $debug) {
        // Indicar el proyecto con el que quiero trabajar
        //
        //
        //
        //
        $this->proyecto = "ipa";
//        echo($_SERVER['SYMFONY__ENV__PROYECTO']);
        //
        //
        //
        //
        parent::__construct($environment, $debug);
    }
    
    public function getProyecto(){
        return $this->proyecto;
    }
//  -----------------------------------------------------
    
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),            
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            new Sonata\AdminBundle\SonataAdminBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new SunCat\MobileDetectBundle\MobileDetectBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new FM\ElfinderBundle\FMElfinderBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),         
            
//          Bundles del proyecto saquito .-
            new APD\Saquito\MainBundle\APDSaquitoMainBundle(),
            new APD\Saquito\NoticiasBundle\APDSaquitoNoticiasBundle(),
            new APD\Saquito\SeguridadBundle\APDSaquitoSeguridadBundle(),
            new APD\Saquito\PaginasBundle\APDSaquitoPaginasBundle(),
            new APD\Saquito\InitializrBundle\APDSaquitoInitializrBundle(),
            new APD\Saquito\MultimediaBundle\APDSaquitoMultimediaBundle(),
        );
        
//          Carga Bundles del proyecto activo.-
        switch ($this->getProyecto()) {
            case "ejemplo":
                break;
            case "ipa":
                $bundles[] = new APD\Ipa\InitializrBundle\APDIpaInitializrBundle();
                $bundles[] = new APD\Ipa\PropioBundle\APDIpaPropioBundle();
                $bundles[] = new APD\Ipa\MainBundle\APDIpaMainBundle();
                $bundles[] = new APD\Ipa\NoticiasBundle\APDIpaNoticiasBundle();
                $bundles[] = new APD\Ipa\PaginasBundle\APDIpaPaginasBundle();
                $bundles[] = new APD\Ipa\SeguridadBundle\APDIpaSeguridadBundle();
                $bundles[] = new APD\Ipa\MiBundle\APDIpaMiBundle();
                $bundles[] = new APD\Ipa\JornadaBundle\APDIpaJornadaBundle();
                break;
            case "playaunionweb":
                $bundles[] = new APD\PlayaUnionWeb\EstablecimientosBundle\APDPlayaUnionWebEstablecimientosBundle();
                $bundles[] = new APD\PlayaUnionWeb\InitializrBundle\APDPlayaUnionWebInitializrBundle();
                $bundles[] = new APD\PlayaUnionWeb\MainBundle\APDPlayaUnionWebMainBundle();
                $bundles[] = new APD\PlayaUnionWeb\NoticiasBundle\APDPlayaUnionWebNoticiasBundle();
                $bundles[] = new APD\PlayaUnionWeb\PaginasBundle\APDPlayaUnionWebPaginasBundle();
                $bundles[] = new APD\PlayaUnionWeb\PropioBundle\APDPlayaUnionWebPropioBundle();
                break;
            case "cultura":
                $bundles[] = new APD\Cultura\CulturaBundle\APDCulturaCulturaBundle();
                $bundles[] = new APD\Cultura\InitializrBundle\APDCulturaInitializrBundle();
                $bundles[] = new APD\Cultura\MainBundle\APDCulturaMainBundle();
                $bundles[] = new APD\Cultura\NoticiasBundle\APDCulturaNoticiasBundle();
                $bundles[] = new APD\Cultura\PaginasBundle\APDCulturaPaginasBundle();
                $bundles[] = new APD\Cultura\CatalogosBundle\APDCulturaCatalogosBundle();
                $bundles[] = new APD\Cultura\SeguridadBundle\APDCulturaSeguridadBundle();
                $bundles[] = new APD\Cultura\AgendaBundle\APDCulturaAgendaBundle();
                break;
        }        

        if (in_array($this->getEnvironment(), array('dev', 'test','apdweb'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
        $loader->load($this->getRootDir().'/config/proyecto_'.$this->getProyecto().'.yml');
    }
}
